
import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:sydney_haarsalon/app_router.dart';
import 'package:sydney_haarsalon/service/navigation_service.dart';
import 'package:sydney_haarsalon/splash_screen.dart';
import 'package:sydney_haarsalon/state_container/state_container.dart';
import 'package:sydney_haarsalon/utils/utility.dart';

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
}

GetIt locator = GetIt.instance;
// Run first apps open
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  FirebaseApp app = await Firebase.initializeApp();
  assert(app != null);
  print('Initialized default app $app');
  /* Background push notification message handler */
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  /* Initialize Local notification */
  await initializeLocalNotification();
  initializeDateFormatting().then((_) => runApp(StateContainer(child: App())));
}
Map<String,dynamic> mapLaunchData=Map();
const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'sydneyhaarsaloon', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

Future<void> initializeLocalNotification() async{
  /// Create an Android Notification Channel.
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('notification_icon');
  final IOSInitializationSettings initializationSettingsIOS =
  IOSInitializationSettings(
      onDidReceiveLocalNotification: onDidReceiveLocalNotification);
  final MacOSInitializationSettings initializationSettingsMacOS =
  MacOSInitializationSettings();
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: initializationSettingsMacOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: selectNotification);
}
Future onDidReceiveLocalNotification(
    int id, String title, String body, String payload) async {
  // display a dialog with the notification details, tap ok to go to another page
  debugPrint('notification payload onDidReceiveLocalNotification : $payload');
}
Future selectNotification(String payload) async {
  if (payload != null) {
    debugPrint('notification payload selectNotification : $payload');
    Utility.checkForNotification(jsonDecode(payload));
  }else{
    debugPrint('notification payload selectNotification : $payload');
  }

}
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}


/// Set orienttation
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /// To set orientation always portrait
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    ///Set color status bar
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return new MaterialApp(
      navigatorKey: locator<NavigationService>().navigatorKey,
      title: "Sydney Haarsaloon",
      theme: ThemeData(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          primaryColorLight: Colors.white,
          primaryColorBrightness: Brightness.light,
          primaryColor: Colors.white),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      initialRoute: SplashScreen.routeName,
      /*home: WaitingListConfirmScreen(),
      initialRoute: WaitingListConfirmScreen.routeName,*/
      onGenerateRoute: AppRouter.generateRoute,
    );
  }
}

/// Component UI

