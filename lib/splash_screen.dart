import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sydney_haarsalon/api/api_request.dart';
import 'package:sydney_haarsalon/firebase_notification/firebase_notification_handler.dart';
import 'package:sydney_haarsalon/main.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/login_or_signup_screen.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/utility.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName = "/";

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

// Component UI
class _SplashScreenState extends State<SplashScreen> {
  @override

  /// Setting duration in splash screen
  startTime() async {
    return new Timer(Duration(milliseconds: 4000), NavigatorPage);
  }

  /// To navigate layout change
  void NavigatorPage() {
    if (mapLaunchData != null && mapLaunchData.length > 0) {
      Utility.checkForNotification(mapLaunchData);
    } else {
      Navigator.of(context).pushReplacementNamed(LoginOrSignupScreen.routeName);
    }
  }

  /// Declare startTime to InitState
  @override
  void initState() {
    initSplashScreen();
    super.initState();
  }

  void initSplashScreen() async {

    FirebaseNotificationHandler().setUpFirebase((fToken) async {
      await ApiRequest.postStoreToken(firebaseRegToken: fToken);
      startTime();
    });
      // startTime();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Code Create UI Splash Screen
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: MyColors.splshBackgroundColor,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/gif/splashscreen.gif'), fit: BoxFit.cover),
        ),
      ) /*LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            width: constraints.minWidth,
            height: constraints.minHeight,
            color: Colors.blue,
            child: Image.asset(
              "assets/gif/splashscreen.gif",
//              height: constraints.minHeight,
//              width: constraints.minWidth,
              fit: BoxFit.fitHeight,
            ),
          );
        },
      )*/ /*Container(
        color: MyColors.borderColor,
        child: Center(
          child: Shimmer.fromColors(
            baseColor: MyColors.backgroundColor,
            highlightColor: MyColors.borderColor,
            child: Container(
              width: screenWidth/2,
              child: Image.asset("assets/icons/logo.png",)
            ),
          ),
        ),
      )*/
      ,
    );
  }
}
