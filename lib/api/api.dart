class API{
//  http://work-updates.com/salon/public/api/get-categories
  /*static const String BASE = "https://reqres.in/";
  static const String LOGIN_USER = BASE+"api/login";*/
//  http://work-updates.com/salon/public/api/get-categories
//  http://work-updates.com/salon/public/api/get-services?id_category=1
//  http://work-updates.com/salon/public/api/get-services?id_category=1
//  http://work-updates.com/salon/public/api/get-stylists?id_services=1

  // static const String BASE = "http://work-updates.com/salon/public/";
  static const String BASE = "https://youdidnotexpectthistobehere.nl/inhere/public/";
  static const String GET_CATEGORIES = BASE+"api/get-categories";
  static const String GET_SERVICES = BASE+"api/get-services/{id_category}";
  static const String GET_BOOKING_DETAILS = BASE+"api/booking-detail/{booking_id}";
  static const String GET_CMS_PAGE = BASE+"api/cms";
  static const String GET_SOCIAL_PAGE  = BASE+"api/social";
  // static const String GET_STYLISTS = BASE+"api/get-stylists";
  static const String GET_STYLISTS = BASE+"api/get-stylist-using-service/{id_services}";
  // http://www.work-updates.com/salon/public/api/get-stylist-using-service/3
  static const String GET_CURRENT_MONTH_SLOT = BASE+"api/get-currunt-month-slots";
  static const String POST_BOOK_ORDER = BASE+"api/booking";
  static const String POST_WAITING_LIST_BOOK_ORDER = BASE+"api/waiting-booking";
  static const String POST_STORE_TOKEN = BASE+"api/store-token";
  static const String POST_BOOKING_STATUS_UPDATE = BASE+"api/booking-status-update";


}
