import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:sydney_haarsalon/api/api.dart';
import 'package:http/http.dart' as http;
import 'package:sydney_haarsalon/utils/utility.dart';

class ApiRequest{
  static Future<Map<String, dynamic>> fetchCategories() async {
    var client = http.Client();
    String strAPI = API.GET_CATEGORIES;
    print("API : fetchCategory : ${strAPI}");
    try {
      final response = await client.get(Uri.parse(strAPI));

      if (response.statusCode == 200 || response.statusCode == 201) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return json.decode(response.body);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load categories');
      }
    }on DioError catch(e){
      throw Exception(e.response);
    }catch(e){
      throw Exception(e.toString());
    }
  }
  static Future<Map<String, dynamic>> fetchServices(category) async {

    var client = http.Client();
    String strAPI = API.GET_SERVICES;
    strAPI = strAPI.replaceAll('{id_category}', 'id_category=${category['id']}');
    print("API : fetchServices : ${strAPI}");
    try {
      final response = await client.get(Uri.parse(strAPI));

      if (response.statusCode == 200 || response.statusCode == 201) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return json.decode(response.body);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load services');
      }
    }on DioError catch(e){
      throw Exception(e.response);
    }catch(e){
      throw Exception(e.toString());
    }

  }

  static Future<Map<String, dynamic>> fetchBookingDetails({String id_booking}) async {

    var client = http.Client();
    String strAPI = API.GET_BOOKING_DETAILS;
    strAPI = strAPI.replaceAll('{booking_id}', id_booking);
    print("API : fetchBookingDetails : ${strAPI}");
    try {
      final response = await client.get(Uri.parse(strAPI));

      if (response.statusCode == 200 || response.statusCode == 201) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return json.decode(response.body);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load booking details');
      }
    }on DioError catch(e){
      throw Exception(e.response);
    }catch(e){
      throw Exception(e.toString());
    }

  }
  static Future<Map<String, dynamic>> fetchCmsPage() async {

    var client = http.Client();
    String strAPI = API.GET_CMS_PAGE;
    print("API : fetchCmsPage : ${strAPI}");

    try {
      final response = await client.get(Uri.parse(strAPI));

      if (response.statusCode == 200 || response.statusCode == 201) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return json.decode(response.body);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load categories');
      }
    }on DioError catch(e){
      throw Exception(e.response);
    }catch(e){
      throw Exception(e.toString());
    }

  }

  static Future<Map<String, dynamic>> fetchSocialPage() async {

    var client = http.Client();
    String strAPI = API.GET_SOCIAL_PAGE;
    print("API : fetchSocialPage : ${strAPI}");
    final response = await client.get(Uri.parse(strAPI));

    try {
      if (response.statusCode == 200 || response.statusCode == 201) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return json.decode(response.body);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load categories');
      }
    }on DioError catch(e){
      throw Exception(e.response);
    }catch(e){
      throw Exception(e.toString());
    }

  }
  static Future<Map<String, dynamic>> fetchStylists(service) async {

    var client = http.Client();
    String strAPI = API.GET_STYLISTS;//+"/${service['id']}";
    strAPI = strAPI.replaceAll('{id_services}', "${service['id']}");
    print("API : fetchStylists : ${strAPI}");

    try {
      final response = await client.get(Uri.parse(strAPI));

      if (response.statusCode == 200 || response.statusCode == 201) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return json.decode(response.body);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load categories');
      }
    }on DioError catch(e){
      throw Exception(e.response);
    }catch(e){
      throw Exception(e.toString());
    }

  }
  static Future<Map<String, dynamic>> fetchCurrentMonthSlots({String idStylist,String idService,String day,String month,String year}) async {

    String api_str = API.GET_CURRENT_MONTH_SLOT;
    print("API : GET_CURRENT_MONTH_SLOT : ${api_str}");

    Map<String,dynamic> mapFormData = Map();
    mapFormData["service_id"] = idService;
    mapFormData["stylist_id"] = idStylist;
    mapFormData["month"] = month;
    mapFormData["year"] = year;
    mapFormData["day"] = day;
    debugPrint("API : Params : ${mapFormData.toString()}");
    try {
      Response response = await Dio().post(api_str, data: mapFormData);
      final jsonData = json.decode(response.toString());
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed : ${jsonData.toString()}');
      }
    } on DioError catch(e){
      throw Exception(e.response);
    }catch (e) {
      throw Exception(e.toString());
    }



  }

  static Future<Map<String, dynamic>> postBookOrder({Map<String,dynamic> mData}) async {

    String api_str = API.POST_BOOK_ORDER;
    print("API : POST_BOOK_ORDER : ${api_str}");

    debugPrint("API : Params : ${mData.toString()}");
    FormData formData = FormData.fromMap(mData);
    try {
      Response response = await Dio().post(api_str, data: formData);
      final jsonData = json.decode(response.toString());
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception(jsonData.toString());
      }
    } on DioError catch(e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.

      if(e.response !=null) {
        print("Data : ${e.response.data}");
        print("Headers : ${e.response.headers}");
      } else{
        // Something happened in setting up or sending the request that triggered an Error
        print("Messaage ${e.message}");
      }
    }catch(e){
      throw Exception(e.toString());
    }



  }
  static Future<Map<String, dynamic>> postBookingStatusUpdate({String booking_id,String status}) async {

    String api_str = API.POST_BOOKING_STATUS_UPDATE;
    print("API : POST_BOOKING_STATUS_UPDATE : ${api_str}");

    Map<String,dynamic> mData = Map();
    mData['booking_id'] = booking_id;
    mData['status'] = status;
    debugPrint("API : Params : ${mData.toString()}");
    FormData formData = FormData.fromMap(mData);
    try {
      Response response = await Dio().post(api_str, data: formData);
      final jsonData = json.decode(response.toString());
      // if (response.statusCode == 201) {
      //   final jsonData = json.decode(response.toString());
      var map=Map<String, dynamic>.from(jsonData);
      return map;
      // } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      // throw Exception(jsonData.toString());
      // }
    } on DioError catch(e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.

      if(e.response !=null) {
        print("Data : ${e.response.data}");
        print("Headers : ${e.response.headers}");
      } else{
        // Something happened in setting up or sending the request that triggered an Error
        print("Messaage ${e.message}");
      }
    }catch(e){
      throw Exception(e.toString());
    }



  }
  static Future<Map<String, dynamic>> postStoreToken({String firebaseRegToken}) async {

    String api_str = API.POST_STORE_TOKEN;
    print("API : POST_STORE_TOKEN : ${api_str}");

    Map<String,dynamic> mData = Map();
    mData['device_token'] = await Utility.getDeviceUUID();
    mData['firebase_token'] = firebaseRegToken;
    debugPrint("API : Params : ${mData.toString()}");
    FormData formData = FormData.fromMap(mData);
    try {
      Response response = await Dio().post(api_str, data: formData);
      final jsonData = json.decode(response.toString());
      // if (response.statusCode == 201) {
      //   final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      // } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        // throw Exception(jsonData.toString());
      // }
    } on DioError catch(e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.

      if(e.response !=null) {
        print("Data : ${e.response.data}");
        print("Headers : ${e.response.headers}");
      } else{
        // Something happened in setting up or sending the request that triggered an Error
        print("Messaage ${e.message}");
      }
    }catch(e){
      throw Exception(e.toString());
    }



  }

  static Future<Map<String, dynamic>> postWatinglistBookOrder({Map<String,dynamic> mData}) async {

    String api_str = API.POST_WAITING_LIST_BOOK_ORDER;
    print("API : POST_WAITING_LIST_BOOK_ORDER : ${api_str}");

    debugPrint("API : Params : ${mData.toString()}");
    FormData formData = FormData.fromMap(mData);
    try {
      Response response = await Dio().post(api_str, data: formData);
      final jsonData = json.decode(response.toString());
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception(jsonData.toString());
      }
    } on DioError catch(e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.

      if(e.response !=null) {
        print("Data : ${e.response.data}");
        print("Headers : ${e.response.headers}");
      } else{
        // Something happened in setting up or sending the request that triggered an Error
        print("Messaage ${e.message}");
      }
    }catch(e){
      throw Exception(e.toString());
    }



  }
}