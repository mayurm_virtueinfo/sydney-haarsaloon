import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/splash_screen.dart';
import 'package:sydney_haarsalon/ui/home/booking_screen.dart';
import 'package:sydney_haarsalon/ui/home/get_categories.dart';
import 'package:sydney_haarsalon/ui/home/contact.dart';
import 'package:sydney_haarsalon/ui/home/get_services.dart';
import 'package:sydney_haarsalon/ui/home/get_stylists.dart';
import 'package:sydney_haarsalon/ui/home/home_screen.dart';
import 'package:sydney_haarsalon/ui/home/how_app_works.dart';
import 'package:sydney_haarsalon/ui/home/booking_confirmed.dart';
import 'package:sydney_haarsalon/ui/home/select_date_and_time.dart';
import 'package:sydney_haarsalon/ui/home/settings_screen.dart';
import 'package:sydney_haarsalon/ui/home/terms_and_privacy.dart';
import 'package:sydney_haarsalon/ui/home/waiting_list_confirm_screen.dart';
import 'package:sydney_haarsalon/ui/home/waitinglist_screen.dart';
import 'package:sydney_haarsalon/ui/home/waiting_confirmed.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/login_or_signup_screen.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/login_screen.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/registration_screen.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case SplashScreen.routeName:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case LoginScreen.routeName:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case LoginOrSignupScreen.routeName:
        return MaterialPageRoute(builder: (_) => LoginOrSignupScreen());
      case HomeScreen.routeName:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case GetCategories.routeName:
        return MaterialPageRoute(builder: (_) => GetCategories());
      case HowAppWorks.routeName:
        List<dynamic> lstArgs = args;
        String title = lstArgs[0];
        String content = lstArgs[1];
        return MaterialPageRoute(builder: (_) => HowAppWorks(title: title,content: content,));
      case TermsAndPrivacy.routeName:
        List<dynamic> lstArgs = args;
        String title = lstArgs[0];
        String content = lstArgs[1];
        return MaterialPageRoute(builder: (_) => TermsAndPrivacy(title: title,content: content,));
      case Contact.routeName:
        List<dynamic> lstArgs = args;
        String title = lstArgs[0];
        String content = lstArgs[1];
        return MaterialPageRoute(builder: (_) => Contact(title: title,content: content,));
      case SettingsScreen.routeName:
        return MaterialPageRoute(builder: (_) => SettingsScreen());
      case GetServices.routeName:
        List<dynamic> lstArgs = args;
        final category = lstArgs[0];
        return MaterialPageRoute(builder: (_) => GetServices(category: category,));
      case GetStylists.routeName:
        List<dynamic> lstArgs = args;
        final service = lstArgs[0];
        final category = lstArgs[1];
        return MaterialPageRoute(builder: (_) => GetStylists(service: service,category: category,));
      case SelectDateAndTime.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => SelectDateAndTime(stylist: lstArgs[0],service: lstArgs[1],category: lstArgs[2],));
      case RegistrationScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => RegistrationScreen());
      case WaitingConfirmed.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => WaitingConfirmed(date: lstArgs[0],time: lstArgs[1],));
      case WaitinglistScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => WaitinglistScreen(
          category: lstArgs[0],
          service: lstArgs[1],
          stylish: lstArgs[2],
          date: lstArgs[3],
          time: lstArgs[4],
          allSlotsBooked: lstArgs[5],
        ));
      case BookingConfirmed.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => BookingConfirmed());
      case WaitingListConfirmScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => WaitingListConfirmScreen(idCategory: lstArgs[0],));
      case BookingScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => BookingScreen(
          category: lstArgs[0],
          service: lstArgs[1],
          stylish: lstArgs[2],
          date: lstArgs[3],
          time: lstArgs[4],
        ));

    }
  }
}
