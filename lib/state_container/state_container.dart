import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class StateContainer extends StatefulWidget {
  final Widget child;
  final List<dynamic> cmsPage;
  final List<dynamic> socialPage;

  StateContainer({
    @required this.child,
    this.cmsPage,
    this.socialPage
  });

  static StateContainerState of(BuildContext context) {

    return context.dependOnInheritedWidgetOfExactType<_InheritedStateContainer>().data;
  }

  @override
  StateContainerState createState() => new StateContainerState();
}

class StateContainerState extends State<StateContainer> {
  List<dynamic> cmsPage;
  List<dynamic> socialPage;

  @override
  Widget build(BuildContext context) {
    return new _InheritedStateContainer(
      data: this,
      child: widget.child,
    );
  }


  void updateCMSPage(mCmsPage) {
    setState(() {
      this.cmsPage = mCmsPage;
    });
  }

  void updateSocialPage(mSocialPage) {
    setState(() {
      this.socialPage = mSocialPage;
    });
  }

}

class _InheritedStateContainer extends InheritedWidget {
  final StateContainerState data;

  _InheritedStateContainer({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedStateContainer old) => true;
}