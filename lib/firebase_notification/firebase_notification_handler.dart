import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:sydney_haarsalon/main.dart';
import 'package:sydney_haarsalon/utils/utility.dart';

class FirebaseNotificationHandler {

  void setUpFirebase(Function onTokenReceived) {

    firebaseCloudMessagingListeners(onTokenReceived);
  }

  void firebaseCloudMessagingListeners(onTokenReceived) async {
    // if (Platform.isIOS) iOS_Permission();

    NotificationSettings settings = await FirebaseMessaging.instance.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    print('User granted permission: ${settings.authorizationStatus}');


    try {
      FirebaseMessaging.instance.getToken().then((token) {
        print('token : ${token}');
        onTokenReceived(token);
      });

    } catch (e) {
      onTokenReceived(null);
      debugPrint("Error storing firebase token : ${e.toString()}");
    }
    /*_firebaseMessaging.getAPNSToken().then((token) async {
      print('token : ${token}');
      try {
        onTokenReceived(token);
      } catch (e) {
        onTokenReceived(null);
        debugPrint("Error storing firebase token : ${e.toString()}");
      }
    });*/
    FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage message) {
      // Will be called when app is terminated
      debugPrint("pns-status-getInitialMessage : ${message.toString()}");
      if(message != null && message.data != null){
        if(message.data == null){
          return;
        }
        mapLaunchData = message.data;
      }
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // Will be called when app is in forground state
      debugPrint("pns-status-onMessage");
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      debugPrint(message.notification.title);

      // If `onMessage` is triggered with a notification, construct our own
      // local notification to show to users using the created channel.
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                icon: android?.smallIcon,
                // other properties...
              ),
            ),
          payload: jsonEncode(message.data)
        );

        /*const String groupKey = 'com.android.example.WORK_EMAIL';
        const String groupChannelId = 'grouped channel id';
        const String groupChannelName = 'grouped channel name';
        const String groupChannelDescription = 'grouped channel description';
// example based on https://developer.android.com/training/notify-user/group.html
        const AndroidNotificationDetails firstNotificationAndroidSpecifics =
        AndroidNotificationDetails(
            groupChannelId, groupChannelName, groupChannelDescription,
            importance: Importance.max,
            priority: Priority.high,
            groupKey: groupKey);
        const NotificationDetails firstNotificationPlatformSpecifics =
        NotificationDetails(android: firstNotificationAndroidSpecifics);
        flutterLocalNotificationsPlugin.show(1, 'Alex Faarborg',
            'You will not believe...', firstNotificationPlatformSpecifics);
        const AndroidNotificationDetails secondNotificationAndroidSpecifics =
        AndroidNotificationDetails(
            groupChannelId, groupChannelName, groupChannelDescription,
            importance: Importance.max,
            priority: Priority.high,
            groupKey: groupKey);
        const NotificationDetails secondNotificationPlatformSpecifics =
        NotificationDetails(android: secondNotificationAndroidSpecifics);
        flutterLocalNotificationsPlugin.show(
            2,
            'Jeff Chang',
            'Please join us to celebrate the...',
            secondNotificationPlatformSpecifics);*/


      }
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      // Will be called when app is in background state
      debugPrint("pns-status-onMessageOpenedApp : ${message.notification}");
      debugPrint("pns-status-onMessageOpenedApp : ${message.data}");
      if(message.data == null){
        return;
      }
      Utility.checkForNotification(message.data);
    });
  }

/*void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }*/
/*

  void fcmMsgOnResume(message) {
    try {
      final data = message['data'];
      String screen = data['screen'];

      if (screen == 'PersonalChat') {
        String appointmentType = data['appointmentType'];
        AppointmentType apType;
        if (appointmentType == AppointmentType.SCHEDULED_APPOINTMENT.toString()) {
          apType = AppointmentType.SCHEDULED_APPOINTMENT;
        } else {
          apType = AppointmentType.PAST_APPOINTMENT;
        }
        String userId = data['id_doctor'];
        Map<String, dynamic> patient = json.decode(data['patient']);
        String patientId = patient['id_doctor'].toString();
        if (patient['id_doctor'] is String) {
          String pId = patient['id_doctor'];
          patient['id_doctor'] = int.parse(pId);
        }
        ScheduledConversation scheduledConversation = ScheduledConversation.fromMapDynamic(patient);
        locator<NavigationService>().navigatorKey.currentState.pushNamed(PersonalChat.routeName, arguments: [userId, apType, scheduledConversation]);
      }
    } catch (e) {
      debugPrint("parsing error : ${e.toString()}");
    }
  }
*/
/*
  void fcmMsgOnLaunch(message) {
    try {
      final data = message['data'];
      String screen = data['screen'];

      if (screen == 'PersonalChat') {
        String appointmentType = data['appointmentType'];
        AppointmentType apType;
        if (appointmentType == AppointmentType.SCHEDULED_APPOINTMENT.toString()) {
          apType = AppointmentType.SCHEDULED_APPOINTMENT;
        } else {
          apType = AppointmentType.PAST_APPOINTMENT;
        }
        String userId = data['id_doctor'];
        Map<String, dynamic> patient = json.decode(data['patient']);
        String patientId = patient['id_doctor'].toString();
        if (patient['id_doctor'] is String) {
          String pId = patient['id_doctor'];
          patient['id_doctor'] = int.parse(pId);
        }
        ScheduledConversation scheduledConversation = ScheduledConversation.fromMapDynamic(patient);
        setOnLaunchData(routeName:PersonalChat.routeName,arguments:[userId, apType, scheduledConversation] );
        // locator<NavigationService>().navigatorKey.currentState.pushNamed(PersonalChat.routeName, arguments: [userId, apType, scheduledConversation]);
      }
    } catch (e) {
      debugPrint("parsing error : ${e.toString()}");
    }
  }
  */
}
