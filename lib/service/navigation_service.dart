import 'package:flutter/material.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName,String id_booking) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName,(route)=>false,arguments: [id_booking]);
  }
  Future<dynamic> navigateToSplash(String routeName) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName,(route)=>false);
  }
}
