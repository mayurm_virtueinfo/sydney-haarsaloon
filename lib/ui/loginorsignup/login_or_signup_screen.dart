import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/api/api_request.dart';
import 'package:sydney_haarsalon/state_container/state_container.dart';
import 'package:sydney_haarsalon/ui/home/home_screen.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/login_screen.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/registration_dialog.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/my_toast.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';
import 'package:sydney_haarsalon/widget/app_progressbar.dart';

class LoginOrSignupScreen extends StatefulWidget {
  static const String routeName = "/LoginOrSignupScreen";

  @override
  _LoginOrSignupScreenState createState() => _LoginOrSignupScreenState();
}

enum SelectedLoginButton { LOGIN, REGISTRATION }

class _LoginOrSignupScreenState extends State<LoginOrSignupScreen> {
  SelectedLoginButton selectedLoginButton;

  bool isGettingCMS = false;
  bool isGettingSocial = false;

  @override
  void initState() {
    super.initState();
    checkForPermissiont();
    getCmsPage();
    getSocialPage();
  }

  void getCmsPage() async{
    isGettingCMS = true;
    setState(() {

    });
    try {
      Map<String, dynamic> result = await ApiRequest.fetchCmsPage();
      isGettingCMS = false;
      StateContainer.of(context).updateCMSPage(result['data']);
    }catch(e){
      isGettingCMS = false;
      MyToast.showToast(e.toString(),context);
    }

    setState(() {

    });

  }

  void getSocialPage() async{
    isGettingSocial = true;
    setState(() {

    });
    try {
      Map<String, dynamic> result = await ApiRequest.fetchSocialPage();
      StateContainer.of(context).updateSocialPage(result['data']);
      isGettingSocial = false;
    }catch(e){
      isGettingSocial = false;
      MyToast.showToast(e.message,context);
    }
    setState(() {

    });
  }



  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Center(
            child: SafeArea(
              child: Container(
                  width: screenWidth / 2,
                  child: Image.asset("assets/icons/logo.png")),
            ),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.square(screenWidth / 3),
          //square(screenWidth / 3),
          child: Container(
            color: Colors.blue,
          ),
        ),
      ),
      body: Container(
        constraints: BoxConstraints(
          minWidth: MediaQuery.of(context).size.width,
          minHeight: MediaQuery.of(context).size.height
        ),
        color: MyColors.backgroundColor,
        child: (isGettingSocial && isGettingCMS)?Center(child: Container(
            margin: EdgeInsets.only(bottom: screenWidth / 3),
            child: AppProgressbar())):Column(
          children: <Widget>[
            Expanded(
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.only(bottom: (screenWidth / 3)-30),
                color: MyColors.backgroundColor,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                          onTap: () {
                            onClickLogin();
                          },
                          child: AppButton(
                            backGroundColor:
                            selectedLoginButton == SelectedLoginButton.LOGIN
                                ? MyColors.borderColor
                                : MyColors.backgroundColor,
                            buttonText: Strings.login,
                          )),
                      SizedBox(height: 20),
                      GestureDetector(
                          onTap: () {
                            onClickRegister();
                          },
                          child: AppButton(
                            backGroundColor:
                                selectedLoginButton == SelectedLoginButton.REGISTRATION
                                    ? MyColors.borderColor
                                    : MyColors.backgroundColor,
                            buttonText: Strings.registration,
                          ))
                    ],
                  ),
                ),
              ),
            ),
            AppFooter(),
          ],
        ),
      ),
    );
  }

  void onClickLogin() {
    Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false);
    return;
    setState(() {
      selectedLoginButton = SelectedLoginButton.LOGIN;
      Navigator.of(context).pushNamed(LoginScreen.routeName);
    });
  }

  void onClickRegister() {
    Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false);
    return;
    setState(() {
      selectedLoginButton = SelectedLoginButton.REGISTRATION;
    });
//    Navigator.of(context).pushNamed(HomeScreen.routeName);
    showDialog(
      context: context,
      builder: (BuildContext context) => RegistrationDialog(
        title: "Success",
        description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        buttonText: "Okay",
      ),
    );
  }

  void checkForPermissiont() async{
    /*var status = await Permission.camera.status;
    if (status.isUndetermined) {
      // We didn't ask for permission yet.
    }*/
  }
}
