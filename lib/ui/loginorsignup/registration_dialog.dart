import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/ui/home/home_screen.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/registration_dialog_header.dart';



class RegistrationDialog extends StatefulWidget {
  final String title, description, buttonText;
  final Image image;
  bool isButtonClicked = false;
  RegistrationDialog({
    @required this.title,
    @required this.description,
    @required this.buttonText,
    this.image,
  });

  @override
  _RegistrationDialogState createState() => _RegistrationDialogState();
}

class _RegistrationDialogState extends State<RegistrationDialog> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double paddingLeftRight = screenWidth/10;
    double paddingTopBottom = screenHeight/2.5;
    double dialogWidth = screenWidth - paddingLeftRight;
    double dialogHeight = screenHeight - paddingTopBottom;
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(
              color: MyColors.backgroundColor,
              width: 2.0,
            ),
            color: MyColors.backgroundColor,
            borderRadius: BorderRadius.circular(10)
        ),
//        color: MyColors.backgroundColor,
        width: dialogWidth,
        height: dialogHeight,
        child: Column(
          children: <Widget>[
            RegistrationDialogHeader(),
            Expanded(
              child: Container(
                width: double.infinity,
                color: MyColors.backgroundColor,
                child: Center(
                  child: Form(
                      key: _formKey,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: MyColors.borderColor,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)),
                                  width: (dialogWidth / 5) * 3.4,
                                  height: 50,
                                  child: TextFormField(
                                    // The validator receives the text that the user has entered.
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: Strings.nameLastName,
                                        hintStyle:
                                        TextStyle(color: MyColors.hintColor)),
                                  ),
                                ),
                                Container(
                                    height: 50,
                                    width: 50,
                                    padding: EdgeInsets.all(8),
                                    child: Image.asset("assets/icons/icon_user.png")
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: MyColors.borderColor,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)),
                                  width: (dialogWidth / 5) * 3.4,
                                  height: 50,
                                  child: TextFormField(
                                    // The validator receives the text that the user has entered.
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: Strings.email,
                                        hintStyle:
                                        TextStyle(color: MyColors.hintColor)),
                                  ),
                                ),
                                Container(
                                    height: 50,
                                    width: 50,
                                    padding: EdgeInsets.all(8),
                                    child: Image.asset("assets/icons/icon_email.png")
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: MyColors.borderColor,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)),
                                  width: (dialogWidth / 5) * 3.4,
                                  height: 50,
                                  child: TextFormField(
                                    // The validator receives the text that the user has entered.
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: Strings.password,
                                        hintStyle:
                                        TextStyle(color: MyColors.hintColor)),
                                  ),
                                ),
                                Container(
                                    height: 50,
                                    width: 50,
                                    padding: EdgeInsets.all(8),
                                    child: Image.asset("assets/icons/icon_view.png")
                                )
                              ],
                            ),
                          ])),
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                setState(() {
                  widget.isButtonClicked = true;
                });
                Navigator.pushNamedAndRemoveUntil(context, HomeScreen.routeName, (route) => false);
              },
              child: Container(
                padding: EdgeInsets.all(20),
                child: AppButton(
                  backGroundColor: widget.isButtonClicked?MyColors.borderColor:MyColors.backgroundColor,
                  buttonText: Strings.confirm,
                )
              ),
            )
          ],
        ),
      ),
    );
  }
}