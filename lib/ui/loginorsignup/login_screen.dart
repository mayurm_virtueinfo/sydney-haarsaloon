import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/ui/home/home_screen.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';

class LoginScreen extends StatefulWidget {
  static const String routeName = "/LoginScreen";
  bool isButtonClicked = false;
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_back.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Center(
            child: SafeArea(
              child: Container(
                  width: screenWidth / 2,
                  child: Image.asset("assets/icons/logo.png")),
            ),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.square(screenWidth / 3),
          //square(screenWidth / 3),
          child: Container(
            color: Colors.blue,
          ),
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: MyColors.backgroundColor,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                width: double.infinity,
                color: MyColors.backgroundColor,
                child: Center(
                  child: Form(
                      key: _formKey,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: MyColors.borderColor,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)),
                                  width: (screenWidth / 5) * 3.4,
                                  height: 50,
                                  child: TextFormField(
                                    // The validator receives the text that the user has entered.
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: Strings.userNameOrEmailAddress,
                                        hintStyle:
                                            TextStyle(color: MyColors.hintColor)),
                                  ),
                                ),
                                Container(
                                    height: 50,
                                    width: 50,
                                    padding: EdgeInsets.all(8),
                                    child: Image.asset("assets/icons/icon_user.png")
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
//                                  margin: EdgeInsets.only(top: 20),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: MyColors.borderColor,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)),
                                  width: (screenWidth / 5) * 3.4,
                                  height: 50,
                                  child: TextFormField(
                                    // The validator receives the text that the user has entered.
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: Strings.password,
                                        hintStyle:
                                        TextStyle(color: MyColors.hintColor)),
                                  ),
                                ),
                                Container(
                                    height: 50,
                                    width: 50,
                                    padding: EdgeInsets.all(8),
                                    child: Image.asset("assets/icons/icon_view.png")
                                )
                              ],
                            ),
                          ])),
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                setState(() {
                  widget.isButtonClicked = true;
                });
                Navigator.pushNamedAndRemoveUntil(context, HomeScreen.routeName, (route) => false);
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 50),
                child: AppButton(
                  backGroundColor: widget.isButtonClicked?MyColors.borderColor:MyColors.backgroundColor,
                  buttonText: Strings.confirm,
                )
              ),
            ),
            AppFooter(),
          ],
        ),
      ),
    );
  }
}
