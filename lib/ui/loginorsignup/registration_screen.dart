import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/ui/home/home_screen.dart';
import 'package:sydney_haarsalon/ui/home/waiting_confirmed.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/registration_dialog_header.dart';


class RegistrationScreen extends StatefulWidget {
  static const String routeName = "RegistrationScreen";
  bool isButtonClicked = false;


  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    double screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    double paddingLeftRight = screenWidth / 10;
    double paddingTopBottom = screenHeight / 2.5;
    double dialogWidth = screenWidth - paddingLeftRight;
    double dialogHeight = screenHeight - paddingTopBottom;
    return Scaffold(
      appBar: AppBar(
        title: Text("WACHTLUST",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        centerTitle: true,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_back.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        /* actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: Image.asset("assets/icons/icon_search.png"),
              onPressed: () {},
            ),
          )
        ],*/
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
        /* flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Center(
            child: SafeArea(
              child: Container(
                  width: screenWidth / 2,
                  child: Image.asset("assets/icons/logo.png")),
            ),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.square(screenWidth / 3),
          //square(screenWidth / 3),
          child: Container(
            color: Colors.blue,
          ),
        ),*/
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 40),
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height
          ),
          width: double.infinity,
          color: MyColors.backgroundColor,
          child: Form(
              key: _formKey,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: (dialogWidth / 5) * 3.4,
                          height: 50,
                          child: TextFormField(
                            // The validator receives the text that the user has entered.
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'VOER NAME + LAST NAME IN';
                              }
                              return null;
                            },
                            textAlign: TextAlign.left,
                            decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: MyColors.borderColor),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: MyColors.borderColor),
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: MyColors.borderColor),
                                ),
                                hintText: Strings.nameLastName,
                                hintStyle:
                                TextStyle(color: MyColors.hintColor,fontSize: 20)),
                          ),
                        ),

                        Container(
                            height: 36,
                            width: 36,
                            child: Image.asset(
                                "assets/icons/icon_user.png")
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: (dialogWidth / 5) * 3.4,
                          height: 50,
                          child: TextFormField(
                            // The validator receives the text that the user has entered.
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'VOER EMAIL IN';
                              }
                              return null;
                            },
                            textAlign: TextAlign.left,
                            decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: MyColors.borderColor),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: MyColors.borderColor),
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: MyColors.borderColor),
                                ),
                                hintText: Strings.email,
                                hintStyle:
                                TextStyle(color: MyColors.hintColor,fontSize: 20)),
                          ),
                        ),
                        Container(
                            height: 36,
                            width: 36,
                            child: Image.asset(
                                "assets/icons/icon_email.png")
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: ((dialogWidth / 5) * 4),
                      alignment: Alignment.centerLeft,
                      child: Text("OPMERKING",style: TextStyle(
                        color: MyColors.hintColor,
                        fontSize: 20
                      ),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(left: 5,right: 5),
                      width: (dialogWidth / 5) * 4,
                      height: 150,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        border: Border.all(width: 1,color: MyColors.borderColor),
                        borderRadius: BorderRadius.all(Radius.circular(5))
                      ),
                      child: TextFormField(
                        maxLines: null,
                        keyboardType: TextInputType.multiline,
                        // The validator receives the text that the user has entered.
                       /* validator: (value) {
                          if (value.isEmpty) {
                            return 'VOER OPMERKING IN';
                          }
                          return null;
                        },*/
                        textAlign: TextAlign.start,
                        decoration: InputDecoration(
                          border: InputBorder.none
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          widget.isButtonClicked = true;
                        });
                        if(_formKey.currentState.validate()){
                          FocusScope.of(context).requestFocus(FocusNode());
                        }
                        else{
                          setState(() {
                            widget.isButtonClicked = false;
                          });
                        }
                        /*Navigator.pushNamedAndRemoveUntil(
                            context, HomeScreen.routeName, (
                            route) => false);*/
                      },
                      child: Container(

                        decoration: BoxDecoration(
                            border: Border.all(
                              color: MyColors.borderColor,
                              width: 2.0,
                            ),
                            borderRadius: BorderRadius.circular(10),
                            color: widget.isButtonClicked ? MyColors
                                .borderColor : MyColors.backgroundColor
                        ),
                        height: 50,
                        width: ((dialogWidth / 5) * 3.4) + 50,
                        child: Center(child: Text(Strings.confirm,
                          style: TextStyle(fontSize: 18,
                              fontWeight: FontWeight.bold),)),
                      ),
                    )
                  ])),
        ),
      ),
    );
  }
}