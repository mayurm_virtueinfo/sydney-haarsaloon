import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:sydney_haarsalon/api/api_request.dart';
import 'package:sydney_haarsalon/ui/home/booking_confirmed.dart';
import 'package:sydney_haarsalon/utils/app_styles.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/my_toast.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/utils/utility.dart';
import 'package:sydney_haarsalon/widget/app_progressbar.dart';


class BookingScreen extends StatefulWidget {
  static const String routeName = "BookingScreen";
  bool isButtonClicked = false;

  final category;
  final service;
  final stylish;
  final date;
  final time;

  BookingScreen({this.category,this.service,this.stylish,this.date,this.time});
  @override
  _BookingScreenState createState() => _BookingScreenState();
}

class _BookingScreenState extends State<BookingScreen> {
  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerMobile = TextEditingController();
  // TextEditingController _controllerCity = TextEditingController();
  // TextEditingController _controllerPostcode = TextEditingController();
  // TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerDescription = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    /*_controllerName.text = "abc";
    _controllerEmail.text = "abc@gmail.com";
    _controllerMobile.text = "1234567890";
    _controllerCity.text = "rajkot";
    _controllerPostcode.text = "360001";
    _controllerAddress.text = "605, Alap-A, Limbda Chowk, Rajkot";
    _controllerDescription.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";*/

  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    double screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    double paddingLeftRight = screenWidth / 10;
    // double paddingTopBottom = screenHeight / 2.5;
    double dialogWidth = screenWidth - paddingLeftRight;
    return Scaffold(
      appBar: AppBar(
        title: Text("Boek uw bestelling",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        centerTitle: true,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_back.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(top: 40),
              constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width,
                minHeight: MediaQuery.of(context).size.height
              ),
              width: double.infinity,
              color: MyColors.backgroundColor,
              child: Form(
                  key: _formKey,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        /*Name*/
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: (dialogWidth / 5) * 3.4,
                              height: 50,
                              child: TextFormField(
                                controller: _controllerName,
                                // The validator receives the text that the user has entered.
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'VOER NAME + LAST NAME IN';
                                  }
                                  return null;
                                },
                                style: AppStyle.styleText,
                                textAlign: TextAlign.left,
                                decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    hintText: Strings.nameLastName,
                                    hintStyle:AppStyle.styleHint
                                ),
                              ),
                            ),

                            Container(
                                height: 36,
                                width: 36,
                                child: Image.asset(
                                    "assets/icons/icon_user.png")
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),

                        /*Email*/
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: (dialogWidth / 5) * 3.4,
                              height: 50,
                              child: TextFormField(
                                controller: _controllerEmail,
                                // The validator receives the text that the user has entered.
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'VOER EMAIL IN';
                                  }else if(!Utility.validateEmail(value)){
                                    return 'ONGELDING E-MAILADRES';
                                  }
                                  return null;
                                },
                                textAlign: TextAlign.left,
                                style: AppStyle.styleText,
                                decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    hintText: Strings.email,
                                    hintStyle:AppStyle.styleHint
                                ),
                              ),
                            ),
                            Container(
                                height: 36,
                                width: 36,
                                child: Image.asset(
                                    "assets/icons/icon_email.png")
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),

                        /*Mobile*/
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: ((dialogWidth / 5) * 3.87),
                              height: 50,
                              child: TextFormField(
                                controller: _controllerMobile,
                                // The validator receives the text that the user has entered.
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'VOER MOBIEL IN';
                                  }
                                  return null;
                                },
                                textAlign: TextAlign.left,
                                maxLength: 10,
                                style: AppStyle.styleText,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  /*  prefixText: "06 - ",
                                  prefixStyle: AppStyle.stylePrefix,*/
                                  counterText: "",
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    hintText: Strings.mobile,
                                    hintStyle:AppStyle.styleHint),
                              ),
                            ),
                            /*Container(
                                height: 36,
                                width: 36,
                                child: Image.asset(
                                    "assets/icons/icon_email.png")
                            )*/
                          ],
                        ),
                        /*SizedBox(
                          height: 30,
                        ),
                        *//*City*//*
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: ((dialogWidth / 5) * 3.87),
                              height: 50,
                              child: TextFormField(
                                controller: _controllerCity,
                                // The validator receives the text that the user has entered.
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'GA DE STAD IN';
                                  }
                                  return null;
                                },
                                textAlign: TextAlign.left,
                                style: AppStyle.styleText,
                                decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    hintText: Strings.city,
                                    hintStyle:AppStyle.styleHint
                                ),
                              ),
                            ),
                            *//*Container(
                                height: 36,
                                width: 36,
                                child: Image.asset(
                                    "assets/icons/icon_email.png")
                            )*//*
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        *//*Zipcode*//*
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: ((dialogWidth / 5) * 3.87),
                              height: 50,
                              child: TextFormField(
                                controller: _controllerPostcode,
                                // The validator receives the text that the user has entered.
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'VUL DE ZIP CODE IN';
                                  }
                                  return null;
                                },
                                style: AppStyle.styleText,
                                textAlign: TextAlign.left,
                                maxLength: 10,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    counterText: "",
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: MyColors.borderColor),
                                    ),
                                    hintText: Strings.zipcode,
                                    hintStyle:AppStyle.styleHint),
                              ),
                            ),

                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        *//*Address*//*
                        Container(
                          width: ((dialogWidth / 5) * 3.87),
                          alignment: Alignment.centerLeft,
                          child: Text("Adres",style: TextStyle(
                              color: MyColors.hintColor,
                              fontSize: 20
                          ),),
                        ),*/
                        /*SizedBox(height: 10,),
                        Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.only(left: 5,right: 5),
                          width: (dialogWidth / 5) * 3.87,
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border.all(width: 1,color: MyColors.borderColor),
                              borderRadius: BorderRadius.all(Radius.circular(5))
                          ),
                          child: TextFormField(
                            controller: _controllerAddress,
                            maxLines: 3,
                            keyboardType: TextInputType.multiline,
                            // The validator receives the text that the user has entered.
                            *//* validator: (value) {
                              if (value.isEmpty) {
                                return 'VOER OPMERKING IN';
                              }
                              return null;
                            },*//*
                            style: AppStyle.styleText,
                            textAlign: TextAlign.start,
                            decoration: InputDecoration(
                                border: InputBorder.none
                            ),
                          ),
                        ),*/
                        SizedBox(
                          height: 20,
                        ),

                        /*Comment*/
                        Container(
                          width: ((dialogWidth / 5) * 3.87),
                          alignment: Alignment.centerLeft,
                          child: Text("OPMERKING",style: TextStyle(
                            color: MyColors.hintColor,
                            fontSize: 20
                          ),),
                        ),
                        SizedBox(height: 10,),
                        Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.only(left: 5,right: 5),
                          width: (dialogWidth / 5) * 3.87,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            border: Border.all(width: 1,color: MyColors.borderColor),
                            borderRadius: BorderRadius.all(Radius.circular(5))
                          ),
                          child: TextFormField(
                            controller: _controllerDescription,
                            maxLines: 4,
                            keyboardType: TextInputType.multiline,
                            // The validator receives the text that the user has entered.
                            /*validator: (value) {
                              if (value.isEmpty) {
                                return 'VOER OPMERKING IN';
                              }
                              return null;
                            },*/
                           style: AppStyle.styleText,
                            textAlign: TextAlign.start,
                            decoration: InputDecoration(
                              border: InputBorder.none
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),

                        /*Conform button*/
                        GestureDetector(
                          onTap: () async{

                            setState(() {
                              widget.isButtonClicked = true;
                            });
                            if(_formKey.currentState.validate()){
                              setState(() {
                                isLoading = true;
                              });
                              FocusScope.of(context).requestFocus(FocusNode());
                              Map<String,dynamic> mapOrder = Map();

                              mapOrder['id_category'] =  "${widget.category['id']}";
                              mapOrder['id_service'] = "${widget.service['id']}";
                              mapOrder['id_stylist'] = "${widget.stylish['id']}";
                              mapOrder['price'] = "${widget.service['price']}";
                              mapOrder['name'] = _controllerName.text;;
                              mapOrder['mobile'] = '${_controllerMobile.text}';
                              mapOrder['email'] = _controllerEmail.text;
                              mapOrder['description'] = _controllerDescription.text;

                              DateTime mDate = widget.date;
                              String selDate = DateFormat("yyyy-MM-dd").format(mDate);

                              mapOrder['date'] = selDate;
                              mapOrder['start_time'] = widget.time;
                              String udid = await Utility.getDeviceUUID();
                              mapOrder['udid'] = udid;

                              String dateString = "$selDate ${widget.time}:00";
                              DateTime dateTime = DateTime.parse(dateString);
                              DateTime endDateTime = dateTime.add(Duration(minutes: widget.service['duration']));
                              String endTime = DateFormat("HH:mm").format(endDateTime);

                              mapOrder['end_time'] = endTime;
                              debugPrint("Duration : ${widget.service['duration']}");
                              debugPrint("mapOrder : ${mapOrder.toString()}");
                              try {
                                final result = await ApiRequest.postBookOrder(mData: mapOrder);
                                if(result['data'] is Map){
                                  Navigator.of(context).pushNamedAndRemoveUntil(BookingConfirmed.routeName, (route) => false);
                                }
                                debugPrint("Api Result : ${result.toString()}");
                              }catch(e){
                                debugPrint("Error booking order : ${e.toString()}");
                                MyToast.showToast("Error booking order",context);
                                setState(() {
                                  isLoading = false;
                                });
                              }

                            }
                            else{
                              setState(() {
                                widget.isButtonClicked = false;
                              });
                            }
                            /*Navigator.pushNamedAndRemoveUntil(
                                context, HomeScreen.routeName, (
                                route) => false);*/
                          },
                          child: Container(

                            decoration: BoxDecoration(
                                border: Border.all(
                                  color: MyColors.borderColor,
                                  width: 2.0,
                                ),
                                borderRadius: BorderRadius.circular(10),
                                color: widget.isButtonClicked ? MyColors
                                    .borderColor : MyColors.backgroundColor
                            ),
                            height: 50,
                            width: ((dialogWidth / 5) * 3.4) + 50,
                            child: Center(child: Text(Strings.confirm,
                              style: TextStyle(fontSize: 18,
                                  fontWeight: FontWeight.bold),)),
                          ),
                        )
                      ])),
            ),
            isLoading?Container(
              color: Colors.transparent,
              constraints: BoxConstraints(
                  minHeight: MediaQuery.of(context).size.height,
                  minWidth: MediaQuery.of(context).size.width,
              ),
              child:Center(child: AppProgressbar()),
            ):Container()
          ],
        ),
      ),
    );
  }
}