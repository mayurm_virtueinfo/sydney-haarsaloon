import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:sydney_haarsalon/api/api.dart';
import 'package:sydney_haarsalon/api/api_request.dart';
import 'package:sydney_haarsalon/ui/home/booking_screen.dart';
import 'package:sydney_haarsalon/ui/home/waitinglist_screen.dart';
import 'package:sydney_haarsalon/ui/home/waiting_confirmed.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/registration_dialog.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/registration_screen.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/my_toast.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';

import 'package:sydney_haarsalon/widget/app_progressbar.dart';
import 'package:sydney_haarsalon/widget/item_categories.dart';
import 'package:table_calendar/table_calendar.dart';

//https://pub.dev/packages/table_calendar/example
class SelectDateAndTime extends StatefulWidget {
  static const String routeName = "/SelectDateTwo";
  final stylist;
  final service;
  final category;
  bool isButtonClicked = false;

  SelectDateAndTime({this.stylist, this.service, this.category});

  @override
  _SelectDateAndTimeState createState() => _SelectDateAndTimeState();
}

class _SelectDateAndTimeState extends State<SelectDateAndTime> with TickerProviderStateMixin {
  Map<DateTime, List> _events = Map();
  List _selectedEvents;

  AnimationController _animationController;
  CalendarController _calendarController;

  /*Map<DateTime, List> _holidays = {
    DateTime(2020, 9, 10): ['New Year\'s Day'],
    DateTime(2019, 9, 12): ['Epiphany'],
    DateTime(2019, 9, 14): ['Valentine\'s Day'],
    DateTime(2019, 9, 16): ['Easter Sunday'],
    DateTime(2019, 9, 18): ['Easter Monday'],
  };*/
  Map<String, List> _bookedDays = Map();
  List availableDays = [];
  String strButtonConfirm = Strings.confirm;
  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now();
    /*availableDays = [
      _selectedDay,
      _selectedDay.add(Duration(days: 19)),
      _selectedDay.add(Duration(days: 18)),
      _selectedDay.add(Duration(days: 16)),
      _selectedDay.add(Duration(days: 14)),
      _selectedDay.add(Duration(days: 12)),
      _selectedDay.add(Duration(days: 10)),
      _selectedDay.add(Duration(days: 8)),
      _selectedDay.add(Duration(days: 6)),
      _selectedDay.add(Duration(days: 4)),
      _selectedDay.add(Duration(days: 2)),
      _selectedDay.add(Duration(days: 1)),
      _selectedDay.add(Duration(days: 3)),
      _selectedDay.add(Duration(days: 5)),
      _selectedDay.add(Duration(days: 7)),
      _selectedDay.add(Duration(days: 9)),
    ];*/

    /*_events = {
      availableDays[0]: [ '09:15','09:30','09:45','10:00','10:15','10:30','10:45','11:00','11:15','11:30','11:45','12:00','12:15','12:30','12:45','01:00','01:15','01:30','01:45','02:00','02:15','02:30',
    '02:45','03:00','03:15','03:30','03:45','04:00','04:15','04:30','04:45','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00',
    '07:15', '07:30','07:45','08:00','08:15','08:30','08:45','09:00',],
    availableDays[1]: ['08:00','08:15','08:30','08:45','09:00',],
    availableDays[2]: ['06:15','06:30','06:45','07:00','07:15','07:30','07:45',],
    availableDays[3]: ['08:00','06:45','07:00','07:15','08:15','08:30','08:45','09:00',],
    availableDays[4]: ['11:15','11:30','11:45','12:00','12:15','12:30','12:45','01:00','01:15','01:30',],
    availableDays[5]: ['02:00','02:15','02:30','02:45','03:00'],
    availableDays[6]: ['12:45','01:00','01:15','01:30','01:45','02:00','02:15','02:30','02:45','03:00','03:15','03:30',],
    availableDays[7]: ['04:30','04:45','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00',],
    availableDays[8]: ['06:15','06:30','06:45','07:00','07:15','07:30','07:45','08:00'],
    availableDays[9]: Set.from([ '07:30','07:45','08:00','08:30','08:45','09:00']).toList(),
    availableDays[10]: ['10:30','10:45','11:00','11:15','11:30','11:45','12:00'],
    availableDays[11]: ['11:30','11:45','12:00',],
    availableDays[12]: ['01:45','02:00','02:15','02:30','02:45','03:00','03:15','03:30','03:45'],
    availableDays[13]: ['02:15', '02:30','02:45','03:00',],
    availableDays[14]: ['04:45','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00','07:15','07:30','07:45'],
    availableDays[15]: ['04:45','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00','07:15','07:30','07:45'],
    };*/

    _selectedEvents = []; //_events[_selectedDay] ?? [];
    _calendarController = CalendarController();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    debugPrint("idService : ${widget.service['id']}");
    debugPrint('idStylist : ${widget.stylist['id']}');
    initializeSlots(idStylist: '${widget.stylist['id']}',idService: "${widget.service['id']}", day: "${_selectedDay.day}", month: "${_selectedDay.month}", year: "${_selectedDay.year}");

    _animationController.forward();
  }

  bool isLoading = false;

  DateTime convertDateFromString(String strDate) {
    DateTime todayDate = DateTime.parse(strDate);
    return todayDate;
  }

  void initializeSlots({String idStylist,String idService, String day, String month, String year}) async {
    setState(() {
      isLoading = true;
      availableDays.clear();
      _events.clear();
      _selectedEvents.clear();
      bookedSlots.clear();
    });
    try {
      final result = await ApiRequest.fetchCurrentMonthSlots(idStylist: idStylist,idService: idService, day: day, month: month, year: year);
      debugPrint("Result : ${result}");
      Map<String, dynamic> mapAvailableDays = result['availableDays'];

      mapAvailableDays.forEach((key, value) {
        List<String> keySplit = key.split('-');
        String day = keySplit[0];
        if (day.length == 1) {
          day = '0${day}';
        }
        String month = keySplit[1];
        if (month.length == 1) {
          month = '0${month}';
        }
        String year = keySplit[2];
        if (year.length == 1) {
          year = '0${year}';
        }
        String convertDate = '${year}-${month}-${day}';
        debugPrint("ConverDate : ${convertDate}");
        DateTime dateTime = convertDateFromString(convertDate);
        availableDays.add(dateTime);
        if (dateTime.isAfter(DateTime.now())) {
          debugPrint("isAfter");
          Map<String, dynamic> mapSlots = Map.from(value);

          List<String> listSlots = [];
          List<String> listBookedSlots = [];
          mapSlots.forEach((key1, value1) {
            if (key1 == 'booking') {
              List<Map<String, dynamic>> bookedSlots = List.from(value1);
              bookedSlots.forEach((element) {
                listBookedSlots.add(element['start_time']);
              });
//                _events[dateTime] = value1;
            } else {
              listSlots.add(value1);
            }
          });
          _events[dateTime] = listSlots;
          if (listBookedSlots.length > 0) {
            _bookedDays[formatBookedDate(dateTime)] = listBookedSlots;
          }
        }
      });
      debugPrint("events : ${_events.toString()}");
      isLoading = false;
      setState(() {});
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  String formatBookedDate(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd').format(dateTime);
  }

  DateTime _selectedDay;

//  asdf
  List<String> bookedSlots = List();

  void _onDaySelected(DateTime day, List events) {
    print('CALLBACK: _onDaySelected');
    _selectedDay = day;
    _selectedEvents = events;
    _selectedTimeSlot = '';
    widget.isButtonClicked = false;
    debugPrint("selectedSlots : ${_selectedEvents}");
    bookedSlots = _bookedDays[formatBookedDate(_selectedDay)];
    bookedSlots = bookedSlots??List<String>();

    if(bookedSlots.length == _selectedEvents.length){
      strButtonConfirm = Strings.waitingList;
    }
    else{
      strButtonConfirm = Strings.confirm;
    }
    debugPrint("bookedSlots : ${bookedSlots}");
    setState(() {});
  }

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onVisibleDaysChanged');
    print('CALLBACK: _onVisibleDaysChanged : First : ${first.toString()}');
    print('CALLBACK: _onVisibleDaysChanged : First : ${last.toString()}');
    initializeSlots(idService: "${widget.service['id']}", day: "${first.day}", month: "${first.month}", year: "${first.year}");
  }

  void _onCalendarCreated(DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onCalendarCreated');
  }

  // Simple TableCalendar configuration (using Styles)
  /*Widget _buildTableCalendar() {
    return TableCalendar(
      calendarController: _calendarController,
      events: _events,
//      holidays: _holidays,
      startingDayOfWeek: StartingDayOfWeek.monday,
      calendarStyle: CalendarStyle(
        selectedColor: MyColors.borderColor,
        todayColor: Colors.deepOrange[200],
        markersColor: Colors.brown[700],
        outsideDaysVisible: false,
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle:
            TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Colors.deepOrange[400],
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
    );
  }*/

  // More advanced TableCalendar configuration (using Builders & Styles)
  Widget _buildTableCalendarWithBuilders() {
    return TableCalendar(
//      locale: 'pl_PL',
      locale: 'nl-be',
      calendarController: _calendarController,
      events: _events,
      onHeaderTapped: (focusedDay) {
        debugPrint("focusedDay");
      },
//      initialSelectedDay: DateTime.now(),
//      holidays: _holidays,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      availableGestures: AvailableGestures.none,
      availableCalendarFormats: const {
        CalendarFormat.month: '',
//        CalendarFormat.week: '',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,

        weekendStyle: TextStyle().copyWith(color: MyColors.calendarDateColor),
        weekdayStyle: TextStyle().copyWith(color: MyColors.calendarDateColor),
//        outsideHolidayStyle: TextStyle().copyWith(color: MyColors.calendarDateColor)
//        holidayStyle: TextStyle().copyWith(color: Colors.blue[800]),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle(color: MyColors.weekendColorStyle, fontWeight: FontWeight.bold, fontSize: 18),
        weekdayStyle: TextStyle(color: MyColors.weekendColorStyle, fontWeight: FontWeight.bold, fontSize: 18),
      ),

      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: false,
        titleTextStyle: TextStyle(color: MyColors.weekendColorStyle, fontWeight: FontWeight.bold, fontSize: 18),
        leftChevronIcon: Icon(
          Icons.arrow_back,
          color: MyColors.weekendColorStyle,
        ),
        rightChevronIcon: Icon(
          Icons.arrow_forward,
          color: MyColors.weekendColorStyle,
        ),
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          final result = availableDays.where((element) {
            int diffDays = element.toUtc().difference(date.toUtc()).inDays;
            bool isSame = (diffDays == 0);
            var utcElement = element.toUtc();
            var utcDate = date.toUtc();
            var d1 = DateTime.utc(utcElement.year, utcElement.month, utcElement.day);
            var d2 = DateTime.utc(utcDate.year, utcDate.month, utcDate.day);
            if (d2.compareTo(d1) == 0) {
              return true;
            } else {
              return false;
            }
          }).toList();
          bool isFound = result.length == 0 ? false : true;

          print("IsFound : ${isFound}");
          return isFound
              ? FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_animationController),
            child: Container(
              margin: const EdgeInsets.all(5.0),
//              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
              color: MyColors.borderColor,
              child: Center(
                child: Text(
                  '${date.day}',
                  style: TextStyle().copyWith(fontSize: 14.0),
                ),
              ),
            ),
          )
              : Container(
            margin: const EdgeInsets.all(5.0),
//              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
//              color: MyColors.borderColor,
            child: Center(
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 14.0, color: MyColors.calendarDateColor),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            width: 100,
            height: 100,
            child: Center(
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 14.0, color: MyColors.calendarDateColor),
              ),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 5,
                bottom: 5,
                child: /*Container()*/ _buildEventsMarker(date, events),
              ),
            );
          }

          if (holidays.isNotEmpty) {
            children.add(
              Positioned(
                right: 5,
                top: 3,
                child: _buildHolidaysMarker(date, holidays),
              ),
            );
          }

          return children;
        },
      ),

      onDaySelected: (day, events, holidays) {
        _onDaySelected(day, events);
        _animationController.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
    );
  }

  bool displayCurrentTime = false;

  Widget _buildEventsMarker(DateTime date, List events) {
    return (_selectedDay != null && date.day == _selectedDay.day && date.month == _selectedDay.month && date.year == _selectedDay.year)
        ? AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: MyColors.borderColor /*_calendarController.isSelected(date)
            ? Colors.brown[500]
            : _calendarController.isToday(date)
                ? Colors.brown[300]
                : Colors.blue[400]*/
        ,
      ),
      width: 40.0,
      height: 40.0,
      child: Center(
        child: Text(
          '${/*_selectedEvents.length*/ date.day}',
          style: TextStyle().copyWith(
            color: Colors.black,
            fontSize: 14.0,
          ),
        ),
      ),
    )
        : AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.white /*_calendarController.isSelected(date)
            ? Colors.brown[500]
            : _calendarController.isToday(date)
                ? Colors.brown[300]
                : Colors.blue[400]*/
        ,
      ),
      width: 40.0,
      height: 40.0,
      child: Center(
        child: Text(
          '${/*_selectedEvents.length*/ date.day}',
          style: TextStyle().copyWith(
            color: Colors.black,
            fontSize: 14.0,
          ),
        ),
      ),
    );
  }

  Widget _buildHolidaysMarker(date, events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: MyColors.borderColor /*_calendarController.isSelected(date)
            ? Colors.brown[500]
            : _calendarController.isToday(date)
                ? Colors.brown[300]
                : Colors.blue[400]*/
        ,
      ),
      width: 40.0,
      height: 40.0,
      child: Center(
        child: Text(
          '${/*_selectedEvents.length*/ date.day}',
          style: TextStyle().copyWith(
            color: Colors.black,
            fontSize: 14.0,
          ),
        ),
      ),
    ) /*Icon(
      Icons.add_box,
      size: 20.0,
      color: Colors.green,
    )*/
    ;
  }

  String _selectedTimeSlot = "";
  bool isPrebookedPressed  = false;
  bool isTimeslotPressed = false;
  List<Widget> _buildEventList() {
    return _selectedEvents.asMap().entries.map((entries) {
      int index = entries.key;
      String value = entries.value;

      List<String> result = bookedSlots.where((element) => element == value).toList();
      debugPrint("My List Result : ${result}");
      if(result.length>0){
        return Builder(
          builder: (context) {
            return ChipTheme(
              data: ChipTheme.of(context).copyWith(backgroundColor: MyColors.bookedColor),
              child: ChoiceChip(
                label: Text(value,style: TextStyle(color: Colors.white),),
                selectedColor: MyColors.borderColor,
                selected: _selectedTimeSlot == value,
                onSelected: (selected) {
                  setState(() {
                    widget.isButtonClicked = false;
                    isPrebookedPressed = true;
                    isTimeslotPressed = false;
                    _selectedTimeSlot = value;
                    strButtonConfirm = Strings.waitingList;

                  });
                },
              ),
            );
          },
        );
      }
      else {
        return Builder(
          builder: (context) {
            return ChipTheme(
              data: ChipTheme.of(context).copyWith(backgroundColor: Colors.white),
              child: ChoiceChip(
                label: Text(value),
                selectedColor: MyColors.borderColor,
                selected: _selectedTimeSlot == value,
                onSelected: (selected) {
                  setState(() {
                    widget.isButtonClicked =false;
                    isPrebookedPressed = false;
                    isTimeslotPressed = true;
                    _selectedTimeSlot = value;
                    strButtonConfirm = Strings.confirm;
                  });
                },
              ),
            );
          },
        );
      }

    }).toList();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingMenu = 30;

    return Scaffold(
      appBar: AppBar(
        title: Text("selecteer datum en tijd", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        centerTitle: true,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_back.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: MyColors.backgroundColor,
        child: Stack(
          children: [
            Column(
              children: <Widget>[
                Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        // Switch out 2 lines below to play with TableCalendar's settings
                        //-----------------------
//                _buildTableCalendar(),
                        Container(padding: EdgeInsets.only(left: 35, top: 25, right: 35, bottom: 20), child: _buildTableCalendarWithBuilders()),

                        const SizedBox(height: 8.0),
//                  _buildButtons(),
                        const SizedBox(height: 8.0),
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.only(left: 35, top: 20, right: 35),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    Wrap(
                                      spacing: 10, // gap between adj
                                      runSpacing: -10, // acent chips
                                      children: _buildEventList(), /*<Widget>[

                              ],*/
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        bool allSlotsBooked = false;
                                        if(bookedSlots.length == _selectedEvents.length && _selectedTimeSlot.length == 0){
                                          bool allSlotsBooked = true;
                                          debugPrint("_selectedTimeSlot: ${_selectedTimeSlot}");
                                          debugPrint("Pre-Booked");
                                          Navigator.of(context).pushNamed(WaitinglistScreen.routeName,arguments: [widget.category, widget.service, widget.stylist, _selectedDay, _selectedTimeSlot,allSlotsBooked]);
                                          return;
                                        }
                                        if (_selectedTimeSlot == "") {
                                          MyToast.showToast("Please select timeslot",context);
                                        } else {
                                          setState(() {
                                            widget.isButtonClicked = true;
                                            if(isPrebookedPressed){
                                              Navigator.of(context).pushNamed(WaitinglistScreen.routeName,arguments: [widget.category, widget.service, widget.stylist, _selectedDay, _selectedTimeSlot,allSlotsBooked]);
                                            }
                                            else {
                                              debugPrint("${_selectedDay}");
                                              debugPrint("${_selectedTimeSlot}");
                                              Navigator.of(context).pushNamed(BookingScreen.routeName, arguments: [widget.category, widget.service, widget.stylist, _selectedDay, _selectedTimeSlot]);
                                            }
                                          });
                                        }
                                      },
                                      child: _selectedEvents.length > 0
                                          ? AppButton(
                                        margin: EdgeInsets.only(top: 20),
                                        backGroundColor: widget.isButtonClicked ? MyColors.borderColor : MyColors.backgroundColor,
                                        buttonText: strButtonConfirm,
                                      )
                                          : Container(),
                                    ),
                                  ],
                                ),
                              )),
                        ),

                        SizedBox(
                          height: 20,
                        )
                      ],
                    )),
                AppFooter(),
              ],
            ),
            isLoading
                ? ConstrainedBox(
              constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height, minWidth: MediaQuery.of(context).size.width),
              child: Center(child: AppProgressbar()),
            )
                : Container()
          ],
        ),
      ),
    );
  }
}
