import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';

class SettingsScreen extends StatefulWidget {
  static const String routeName = "/SettingsScreen";
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingMenu = 30;

    double menuSize = (screenWidth/4)+(paddingMenu*2) - 10;

    return Scaffold(
      appBar: AppBar(
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_back.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: Image.asset("assets/icons/icon_search.png"),
              onPressed: () {},
            ),
          )
        ],
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Center(
            child: SafeArea(
              child: Container(
                  width: screenWidth / 2,
                  child: Image.asset("assets/icons/logo.png")),
            ),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.square(screenWidth / 3),
          //square(screenWidth / 3),
          child: Container(
            color: Colors.blue,
          ),
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: MyColors.backgroundColor,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container()
            ),
            AppFooter(),
          ],
        ),
      ),
    );
  }}
