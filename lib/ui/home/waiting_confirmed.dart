import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sydney_haarsalon/api/api.dart';
import 'package:sydney_haarsalon/api/api_request.dart';
import 'package:sydney_haarsalon/ui/home/home_screen.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';

import 'package:sydney_haarsalon/widget/app_progressbar.dart';
import 'package:sydney_haarsalon/widget/item_service.dart';
import 'package:sydney_haarsalon/widget/item_stylists.dart';

class WaitingConfirmed extends StatefulWidget {
  static const String routeName = "/WaitingConfirmed";
  bool isButtonClicked = false;
  String date;
  String time;
  WaitingConfirmed({this.date,this.time});
  @override
  _WaitingConfirmedState createState() => _WaitingConfirmedState();
}


class _WaitingConfirmedState extends State<WaitingConfirmed> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingMenu = 30;
    double screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    double paddingLeftRight = screenWidth / 10;
    double paddingTopBottom = screenHeight / 2.5;
    double dialogWidth = screenWidth - paddingLeftRight;
    double dialogHeight = screenHeight - paddingTopBottom;
    double menuSize = (screenWidth / 4) + (paddingMenu * 2) - 10;

    return WillPopScope(
      onWillPop: () async{
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("WACHTLIJST",style: TextStyle(fontSize: 20,fontWeight:FontWeight.bold)),
          centerTitle: true,
          leading: Container(),
         /* actions: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10),
              child: IconButton(
                icon: Image.asset("assets/icons/icon_search.png"),
                onPressed: () {},
              ),
            )
          ],*/
          backgroundColor: MyColors.backgroundColor,
          elevation: 0.0,
         /* flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Center(
              child: SafeArea(
                child: Container(
                    width: screenWidth / 2,
                    child: Image.asset("assets/icons/logo.png")),
              ),
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.square(screenWidth / 3),
            //square(screenWidth / 3),
            child: Container(
              color: Colors.blue,
            ),
          ),*/
        ),

        body: Container(
          padding: EdgeInsets.only(left: 40,right: 40,top: 40),
          constraints: BoxConstraints(
            minWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height,
          ),
          color: MyColors.backgroundColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Container(

                    child: Text("BEDANKT VOOR DE BEVESTIGING U STAAT OP DE WACHTLIJST VOOR: ${widget.date} U KRIJGT EEN NOTIFICATIE ZODRA ER EEN PLEK VRIJ KOMT HOPELIJK TOT SNEL!",
                        style: _wachlistStyle.copyWith(height: 3),
                        textAlign: TextAlign.center,
                    )),
              ),
             /* SizedBox(height: 40,),
              Text("U STAAT OP DE WACHTLIJST VOOR:",style: _wachlistStyle,textAlign: TextAlign.center),
              SizedBox(height: 40,),
              Text("(INSERT WAITINGLIST DATE)",style: _wachlistStyle,textAlign: TextAlign.center),
              SizedBox(height: 40,),
              Text("U KRIJGT EEN NOTIFICATIE ZODRA ER EEN PLEK VRIJ KOMT",style: _wachlistStyle,textAlign: TextAlign.center),
              SizedBox(height: 40,),
              Text("HOPELIJK TOT SNEL!",style: _wachlistStyle,textAlign: TextAlign.center,),*/
              SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: () async{
                  Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false);
                },
                child: Container(

                  decoration: BoxDecoration(
                      border: Border.all(
                        color: MyColors.borderColor,
                        width: 2.0,
                      ),
                      borderRadius: BorderRadius.circular(10),
                      color: widget.isButtonClicked ? MyColors
                          .borderColor : MyColors.backgroundColor
                  ),
                  height: 50,
                  width: ((dialogWidth / 5) * 3.4) + 50,
                  child: Center(child: Text(Strings.close,
                    style: TextStyle(fontSize: 18,
                        fontWeight: FontWeight.bold),)),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
  TextStyle _wachlistStyle =TextStyle(fontSize: 18,fontWeight: FontWeight.bold,);

}
