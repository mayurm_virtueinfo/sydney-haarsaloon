import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sydney_haarsalon/api/api.dart';
import 'package:sydney_haarsalon/api/api_request.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';

import 'package:sydney_haarsalon/widget/app_progressbar.dart';
import 'package:sydney_haarsalon/widget/item_categories.dart';

class GetCategories extends StatefulWidget {
  static const String routeName = "/GetCategories";

  @override
  _GetCategoriesState createState() => _GetCategoriesState();
}

class Categories {
  final List<Map<String, dynamic>> categories;

  Categories({this.categories});

  factory Categories.fromJson(Map<String, dynamic> mCategories) {
    return Categories(
      categories: mCategories['data'],
    );
  }

  List<Map<String, dynamic>> get getCategories => this.categories;
}

class _GetCategoriesState extends State<GetCategories> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingMenu = 30;

    double menuSize = (screenWidth / 4) + (paddingMenu * 2) - 10;

    return Scaffold(
      appBar: AppBar(
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_back.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: Image.asset("assets/icons/icon_search.png"),
              onPressed: () {},
            ),
          )
        ],
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Center(
            child: SafeArea(
              child: Container(
                  width: screenWidth / 2,
                  child: Image.asset("assets/icons/logo.png")),
            ),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.square(screenWidth / 3),
          //square(screenWidth / 3),
          child: Container(
            color: Colors.blue,
          ),
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: MyColors.backgroundColor,
        child: Column(
          children: <Widget>[
            Expanded(
                child: Column(
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                          width: 50,
                          child:
                              Image.asset("assets/icons/icon_chair_left.png")),
                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: Column(
                          children: <Widget>[
                            Text(
                              Strings.toMakeAnAppointment,
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                            Text(Strings.selectService,
                                style: TextStyle(fontSize: 20)),
                          ],
                        ),
                      ),
                      SizedBox(
                          width: 50,
                          child:
                              Image.asset("assets/icons/icon_chair_right.png"))
                    ],
                  ),
                ),

                SizedBox(height: 50,),
                Expanded(
                  child: FutureBuilder<Map<String, dynamic>>(
                    future: ApiRequest.fetchCategories(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        List<dynamic> dataList = snapshot.data['data'];
                        if(dataList.length == 0){
                          return Container(
                            padding: EdgeInsets.only(bottom: screenWidth/5),
                            constraints: BoxConstraints(
                              maxWidth: MediaQuery.of(context).size.width- (MediaQuery.of(context).size.width/3),
                              maxHeight: MediaQuery.of(context).size.height,
                            ),
                            child: Center(
                              child: Text("Geen categorie beschikbaar",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,height: 1.5),),
                            ),
                          );
                        }
                        return ListView.builder(
                          shrinkWrap: true,
                          itemCount: dataList.length,
                          itemBuilder: (context, index) {
                            Map<String, dynamic> category = dataList[index];
                            debugPrint("received : ${category}");
                            return ItemCategories(category: category,);
                          },
                        );
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      } else {
                        // By default, show a loading spinner.
                        return Center(child: AppProgressbar());
                      }
                    },
                  ),
                )
                /*Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                              onTap: () {},
                              child: AppButton(buttonText: Strings.cutting,)
                          ),
                          SizedBox(height: 20),
                          GestureDetector(
                              onTap: () {},
                              child: AppButton(buttonText: Strings.coloring,)
                          ),
                          SizedBox(height: 20),
                          GestureDetector(
                              onTap: () {},
                              child: AppButton(buttonText: Strings
                                  .cuttingAndColoring,)
                          )
                        ],
                      ),
                    ),*/
              ],
            )),
            AppFooter(),
          ],
        ),
      ),
    );
  }


}
