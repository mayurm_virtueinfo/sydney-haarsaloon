import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/ui/home/home_screen.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';


class BookingConfirmed extends StatefulWidget {
  static const String routeName = "/BookingConfirmed";
  @override
  _BookingConfirmedState createState() => _BookingConfirmedState();
}

class _BookingConfirmedState extends State<BookingConfirmed> {

  /// Setting duration in splash screen
  startTime() async {
    return new Timer(Duration(milliseconds: 3000), NavigatorPage);
  }

  /// To navigate layout change
  void NavigatorPage() async{
    Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false);
  }
  Future<bool> _onBackPressed() {
//    Navigator.of(context).pop(false);
  }


  /// Declare startTime to InitState
  @override
  void initState() {

    super.initState();
    // startTime();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    double screenHeight = MediaQuery
        .of(context)
        .size
        .height;

    double paddingLeftRight = screenWidth / 10;
    double paddingTopBottom = screenHeight / 2.5;
    double dialogWidth = screenWidth - paddingLeftRight;
    double dialogHeight = screenHeight - paddingTopBottom;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            color: MyColors.borderColor.withOpacity(0.8),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[


                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Theme.of(context).primaryColor
                          ),
                          child: Icon(Icons.check,size: 85,color: MyColors.borderColor.withOpacity(0.8),)
                      ),
                      SizedBox(height: 20,),
                      Text("Bedankt voor uw reservering.",textAlign: TextAlign.center,style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                      ),),
                      SizedBox(height: 20,),
                      Text("U ontvangt straks een bevestiging per email van uw afspraak.",textAlign: TextAlign.center,style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                      ),),
                      SizedBox(height: 20,),
                      Text("Tot snel !",textAlign: TextAlign.center,style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                      ),),
                      SizedBox(height: 20,),
                      Text("Met vriendelijke groeten,",textAlign: TextAlign.center,style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                      ),),
                      Text("Sydney Haarsalon",textAlign: TextAlign.center,style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                      ),),
                    ],
                  ),
                ),

                SizedBox(
                  height: 30,
                ),
                GestureDetector(
                  onTap: () async{
                    Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false);
                  },
                  child: Container(

                    decoration: BoxDecoration(
                        border: Border.all(
                          color: MyColors.borderColor,
                          width: 2.0,
                        ),
                        borderRadius: BorderRadius.circular(10),
                        color:  MyColors.borderColor
                    ),
                    height: 50,
                    width: ((dialogWidth / 5) * 3.4) + 50,
                    child: Center(child: Text(Strings.close,
                      style: TextStyle(fontSize: 18,
                          fontWeight: FontWeight.bold),)),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}