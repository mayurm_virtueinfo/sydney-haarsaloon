import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sydney_haarsalon/api/api_request.dart';
import 'package:sydney_haarsalon/main.dart';
import 'package:sydney_haarsalon/service/navigation_service.dart';
import 'package:sydney_haarsalon/ui/home/home_screen.dart';
import 'package:sydney_haarsalon/ui/loginorsignup/login_or_signup_screen.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_progressbar.dart';


class WaitingListConfirmScreen extends StatefulWidget {
  static const String routeName = "/WaitingListConfirmScreen";
  static const String screenName = "WaitingListConfirmScreen";
  String idCategory;

  WaitingListConfirmScreen({this.idCategory});
  @override
  _WaitingListConfirmScreenState createState() => _WaitingListConfirmScreenState();
}
enum SelectedStatusButton { NONE,REJECT, ACCEPT }
class _WaitingListConfirmScreenState extends State<WaitingListConfirmScreen> {

  /// Setting duration in splash screen

  Future<bool> _onBackPressed() {
//    Navigator.of(context).pop(false);
  }
  SelectedStatusButton selectedStatusButton=SelectedStatusButton.NONE;
  bool isLoading = false;

  String bookingDate;
  bool displayApptAvail = true;
  bool displayAcceptAvail = false;
  bool displayRejectAvail = false;
  /// Declare startTime to InitState
  @override
  void initState() {

    super.initState();
    // startTime();
    fetchBookingDetails();
  }

  Map<String,dynamic> dataBooking ;
  void fetchBookingDetails() async{
    setState(() {
      isLoading = true;
    });
    try {
      final result = await ApiRequest.fetchBookingDetails(id_booking: widget.idCategory);
      dataBooking = result['data'];
      String date = dataBooking['date'];
      String startTime = dataBooking['start_time'];
      String dateString ='${date} ${startTime}';
      DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(dateString);
      bookingDate = DateFormat("dd-MM-yyyy hh:mm a").format(tempDate);
      debugPrint("Result : ${dataBooking}");
      setState(() {
        isLoading = false;
      });
    }catch(e){
      setState(() {
        isLoading = false;
      });
      debugPrint("Error getting services : ${e.toString()}");
    }
  }

  void postBookingStatusUpdate(String status) async{
    setState(() {
      isLoading = true;
    });
    try {
      final result = await ApiRequest.postBookingStatusUpdate(booking_id: widget.idCategory,status: status);
      debugPrint("Result status update : ${result}");
      setState(() {
        isLoading = false;

        if(status == 'accepted'){
          displayApptAvail = false;
          displayRejectAvail = false;
          displayAcceptAvail = true;
        }else if(status == 'rejected'){
          displayApptAvail = false;
          displayRejectAvail = true;
          displayAcceptAvail = false;
        }

      });
    }catch(e){
      setState(() {
        isLoading = false;
      });
      debugPrint("Error updating booking status update : ${e.toString()}");
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    double screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    double paddingLeftRight = screenWidth / 10;
    double paddingTopBottom = screenHeight / 2.5;
    double dialogWidth = screenWidth - paddingLeftRight;
    double dialogHeight = screenHeight - paddingTopBottom;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MyColors.backgroundColor,
          elevation: 0.0,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Center(
              child: SafeArea(
                child: Container(
                    width: screenWidth / 2,
                    child: Image.asset("assets/icons/logo.png")),
              ),
            ),
          ),
          leading: Container()/*IconButton(
            icon: Icon(Icons.home),
            onPressed: (){
              locator<NavigationService>().navigateToSplash(LoginOrSignupScreen.routeName);
            },
          )*/,
          bottom: PreferredSize(
            preferredSize: Size.square(screenWidth / 3),
            //square(screenWidth / 3),
            child: Container(
              color: Colors.blue,
            ),
          ),
        ),
        body: Container(
          constraints: BoxConstraints(
              minWidth: MediaQuery.of(context).size.width,
              minHeight: MediaQuery.of(context).size.height
          ),
          color: MyColors.backgroundColor,
          child: isLoading?Center(child: Container(
              margin: EdgeInsets.only(bottom: (screenWidth / 3)),
              child: AppProgressbar()),):Column(
            children: <Widget>[

              Expanded(
                child: Container(
                  width: double.infinity,
                  padding: EdgeInsets.only(bottom: (screenWidth / 3)-30),
                  color: MyColors.backgroundColor,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        displayApptAvail && dataBooking!=null?Container(
                          child:Column(
                            children: [
                              Text('Appointment Available',style: TextStyle(fontSize: 24,fontWeight:FontWeight.bold),),
                              SizedBox(height: 10,),
                              Text('${bookingDate}',style: TextStyle(fontSize: 18)),
                              SizedBox(height: 5,),
                              Text('For, ${dataBooking['service_name']}',style: TextStyle(fontSize: 18)),
                              SizedBox(height: 30,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  GestureDetector(
                                      onTap: () async{
                                        try {
                                          await postBookingStatusUpdate('rejected');
                                        }catch(e){
                                          debugPrint("Error rejecting booking status update : ${e.toString()}");
                                        }
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              color: MyColors.borderColor,
                                              width: 2.0,
                                            ),
                                            borderRadius: BorderRadius.circular(10),
                                            color: selectedStatusButton == SelectedStatusButton.REJECT
                                                ? MyColors.borderColor
                                                : MyColors.backgroundColor
                                        ),
                                        width: screenWidth / 3,
                                        height: 50,
                                        child: Center(child: Text('REJECT',style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),)),
                                      )),
                                  SizedBox(width: 20,),
                                  GestureDetector(
                                      onTap: () async{
                                        try {
                                          await postBookingStatusUpdate('accepted');
                                        }catch(e){
                                          debugPrint("Error accepting booking status update : ${e.toString()}");
                                        }
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              color: MyColors.borderColor,
                                              width: 2.0,
                                            ),
                                            borderRadius: BorderRadius.circular(10),
                                            color: selectedStatusButton == SelectedStatusButton.ACCEPT
                                                ? MyColors.borderColor
                                                : MyColors.backgroundColor
                                        ),
                                        width: screenWidth / 3,
                                        height: 50,
                                        child: Center(child: Text('ACCEPT',style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),)),
                                      ))
                                ],
                              )],
                          ),
                        ):Container(),

                        displayAcceptAvail?Column(
                          children: [
                            Container(
                              width: screenWidth / 1.5,
                              child: Text('Your appointment has been confirmed for ${bookingDate} & ${dataBooking['service_name']}',
                                  style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),
                              textAlign: TextAlign.center ,)),
                            SizedBox(height: 30,),
                            GestureDetector(
                                onTap: () async{
                                 navigateToHome();
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: MyColors.borderColor,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: selectedStatusButton == SelectedStatusButton.REJECT
                                          ? MyColors.borderColor
                                          : MyColors.backgroundColor
                                  ),
                                  width: screenWidth / 3,
                                  height: 50,
                                  child: Center(child: Text('Go Home',style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),)),
                                )),
                          ],
                        ):Container(),

                        displayRejectAvail?Column(
                          children: [
                            Container(
                                width: screenWidth / 1.5,
                                child: Text('Your booking has been rejected',
                                  style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),
                                  textAlign: TextAlign.center ,)),
                            SizedBox(height: 30,),
                            GestureDetector(
                                onTap: () async{
                                  navigateToHome();
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: MyColors.borderColor,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: selectedStatusButton == SelectedStatusButton.REJECT
                                          ? MyColors.borderColor
                                          : MyColors.backgroundColor
                                  ),
                                  width: screenWidth / 3,
                                  height: 50,
                                  child: Center(child: Text('Go Home',style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),)),
                                )),
                          ],
                        ):Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ),
      ),
    );
  }

  void navigateToHome(){
    locator<NavigationService>().navigateToSplash(LoginOrSignupScreen.routeName);
  }
  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Reject"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text("Accept"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("AlertDialog"),
      content: Text("Would you like to accept or reject"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}