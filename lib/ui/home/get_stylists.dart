import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sydney_haarsalon/api/api.dart';
import 'package:sydney_haarsalon/api/api_request.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';

import 'package:sydney_haarsalon/widget/app_progressbar.dart';
import 'package:sydney_haarsalon/widget/item_service.dart';
import 'package:sydney_haarsalon/widget/item_stylists.dart';

class GetStylists extends StatefulWidget {
  static const String routeName = "/GetStylists";
  final service;
  final category;
  GetStylists({this.service,this.category});
  @override
  _GetStylistsState createState() => _GetStylistsState();
}

class Services {
  final List<Map<String, dynamic>> services;

  Services({this.services});

  factory Services.fromJson(Map<String, dynamic> mCategories) {
    return Services(
      services: mCategories['data'],
    );
  }

  List<Map<String, dynamic>> get getCategories => this.services;
}

class _GetStylistsState extends State<GetStylists> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingMenu = 30;

    double menuSize = (screenWidth / 4) + (paddingMenu * 2) - 10;

    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.service['title']}",style: TextStyle(fontSize: 20,fontWeight:FontWeight.bold)),
        centerTitle: true,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_back.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: MyColors.backgroundColor,
        child: Column(
          children: <Widget>[
            Expanded(
                child: Column(
              children: <Widget>[
                Expanded(
                  child: FutureBuilder<Map<String, dynamic>>(
                    future: ApiRequest.fetchStylists(widget.service),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        List<dynamic> dataList = snapshot.data['data'];
                        if(dataList.length == 0){
                          return Container(
                            constraints: BoxConstraints(
                              maxWidth: MediaQuery.of(context).size.width- (MediaQuery.of(context).size.width/3),
                              maxHeight: MediaQuery.of(context).size.height,
                            ),
                            child: Center(
                              child: Text("Geen Stylist beschikbaar voor ${widget.service['title']}",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,height: 1.5),),
                            ),
                          );
                        }
                        return GridView.builder(
                          gridDelegate:
                          new SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2),
                          shrinkWrap: true,
                          itemCount: dataList.length,
                          itemBuilder: (context, index) {
                            final stylist = dataList[index];

                            debugPrint("received stylist: ${stylist}");
                            debugPrint("-----service: ${widget.service}");
                            return InkWell(
                              child: ItemStylist(stylist: stylist,service: widget.service,category: widget.category,)
                            );
                          },
                        );
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      } else {
                        // By default, show a loading spinner.
                        return Center(child: AppProgressbar());
                      }
                    },
                  ),
                )
                /*Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                              onTap: () {},
                              child: AppButton(buttonText: Strings.cutting,)
                          ),
                          SizedBox(height: 20),
                          GestureDetector(
                              onTap: () {},
                              child: AppButton(buttonText: Strings.coloring,)
                          ),
                          SizedBox(height: 20),
                          GestureDetector(
                              onTap: () {},
                              child: AppButton(buttonText: Strings
                                  .cuttingAndColoring,)
                          )
                        ],
                      ),
                    ),*/
              ],
            )),
            AppFooter(),
          ],
        ),
      ),
    );
  }


}
