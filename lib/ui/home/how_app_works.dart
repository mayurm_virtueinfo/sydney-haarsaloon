import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';

class HowAppWorks extends StatefulWidget {
  static const String routeName = "/HowAppWorks";
  String title;
  String content;
  HowAppWorks({this.title,this.content});
  @override
  _HowAppWorksState createState() => _HowAppWorksState();
}

class _HowAppWorksState extends State<HowAppWorks> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingMenu = 30;

    double menuSize = (screenWidth/4)+(paddingMenu*2) - 10;

    return Scaffold(
      appBar: AppBar(
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_back.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        title: Text(widget.title,style: TextStyle(fontSize: 20,fontWeight:FontWeight.bold)),

        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,

      ),
      body: Container(
        padding: EdgeInsets.all(20),
        constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.height,
          maxWidth: MediaQuery.of(context).size.width
        ),
        color: MyColors.backgroundColor,
        child: Column(
          children: <Widget>[
            Html(
              data:widget.content,
              shrinkWrap: true,
              /*style: {
                'p':Style(
                  textAlign: TextAlign.center,
                )
              },*/
            ),
          ],
        ),
      ),
    );
  }}
