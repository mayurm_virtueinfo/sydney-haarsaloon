import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/state_container/state_container.dart';
import 'package:sydney_haarsalon/ui/home/get_categories.dart';
import 'package:sydney_haarsalon/ui/home/contact.dart';
import 'package:sydney_haarsalon/ui/home/how_app_works.dart';
import 'package:sydney_haarsalon/ui/home/booking_confirmed.dart';
import 'package:sydney_haarsalon/ui/home/settings_screen.dart';
import 'package:sydney_haarsalon/ui/home/terms_and_privacy.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/utils/strings.dart';
import 'package:sydney_haarsalon/widget/app_footer.dart';
import 'package:sydney_haarsalon/widget/home_menu_button.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = "/HomeScreen";

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

enum SelectedMenu { BOOK_APPOINTMENT, HOW_APP_WORKS, CONTACT, SETTINGS }

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  SelectedMenu selectedMenu;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingMenu = 30;
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    debugPrint('devicePixelRatio : ${devicePixelRatio}');

    double menuSize = (screenWidth / 4) + (paddingMenu * 2) - 10;

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset("assets/icons/icon_menu.png"),
            onPressed: () {
              _scaffoldKey.currentState.openDrawer();
            },
          ),
        ),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: Image.asset("assets/icons/icon_search.png"),
              onPressed: () {},
            ),
          )
        ],
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Center(
            child: SafeArea(
              child: Container(
                  width: screenWidth / 2,
                  child: Image.asset("assets/icons/logo.png")),
            ),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.square(screenWidth / 3),
          //square(screenWidth / 3),
          child: Container(
            color: Colors.blue,
          ),
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width,
          maxHeight: MediaQuery.of(context).size.height
        ),
        color: MyColors.backgroundColor,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(bottom: (devicePixelRatio * (screenWidth/10))),
                width: double.infinity,
                color: MyColors.backgroundColor,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Wrap(
                        direction: Axis.horizontal,
                        children: <Widget>[
                          HomeMenuButton(
                            isTapped: selectedMenu == SelectedMenu.BOOK_APPOINTMENT?true:false,
                            name: Strings.toMakeAnAppointment,
                            onTap: () {
                              onClickBookAppointment();
                            },
                            backgroundColor: selectedMenu ==
                                SelectedMenu.BOOK_APPOINTMENT
                                ? MyColors.borderColor
                                : MyColors.backgroundColor,
                            imgAssetPath: "assets/icons/icon_chair.png",
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          HomeMenuButton(
                            isTapped: selectedMenu == SelectedMenu.HOW_APP_WORKS?true:false,
                            name: Strings.howTheAppWorks,
                            onTap: () {
                              onClickHowAppWorks();
                            },
                            backgroundColor: selectedMenu ==
                                SelectedMenu.HOW_APP_WORKS
                                ? MyColors.borderColor
                                : MyColors.backgroundColor,
                            imgAssetPath: "assets/icons/icon_question.png",
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        direction: Axis.horizontal,
                        children: <Widget>[
                          HomeMenuButton(
                            isTapped: selectedMenu == SelectedMenu.CONTACT?true:false,
                            name: Strings.contact,
                            onTap: () {
                              onClickContact();
                            },
                            backgroundColor: selectedMenu == SelectedMenu.CONTACT
                                ? MyColors.borderColor
                                : MyColors.backgroundColor,
                            imgAssetPath: "assets/icons/icon_contact.png",
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          HomeMenuButton(
                            isTapped: selectedMenu == SelectedMenu.SETTINGS?true:false,
                            name: Strings.settings,
                            onTap: () {
                              onClickSettings();
                            },
                            backgroundColor: selectedMenu == SelectedMenu.SETTINGS
                                ? MyColors.borderColor
                                : MyColors.backgroundColor,
                            imgAssetPath: "assets/icons/icon_settings.png",
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            AppFooter(),
          ],
        ),
      ),
      drawer: Drawer(
        child: Container(
          color: MyColors.borderColor,
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Container(
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: IconButton(
                          icon: Image.asset("assets/icons/icon_menu.png"),
                          onPressed: () {
                            if (_scaffoldKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Text(
                        Strings.menu,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              GestureDetector(
                onTap: () {
                  if (_scaffoldKey.currentState.isDrawerOpen) {
                    Navigator.pop(context);
                  }
                  onClickBookAppointment();
                },
                child: Container(
                  margin: EdgeInsets.only(left: 20, top: 35),
                  child: Text(
                    Strings.toMakeAnAppointment,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  if (_scaffoldKey.currentState.isDrawerOpen) {
                    Navigator.pop(context);
                  }
                  onClickHowAppWorks();
                },
                child: Container(
                  margin: EdgeInsets.only(left: 20, top: 35),
                  child: Text(
                    Strings.howTheAppWorks,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  if (_scaffoldKey.currentState.isDrawerOpen) {
                    Navigator.pop(context);
                  }
                  onClickContact();
                },
                child: Container(
                  margin: EdgeInsets.only(left: 20, top: 35),
                  child: Text(
                    Strings.contact,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  if (_scaffoldKey.currentState.isDrawerOpen) {
                    Navigator.pop(context);
                  }
                  onClickSettings();
                },
                child: Container(
                  margin: EdgeInsets.only(left: 20, top: 35),
                  child: Text(
                    Strings.settings,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  if (_scaffoldKey.currentState.isDrawerOpen) {
                    Navigator.pop(context);
                  }
                  String title = StateContainer.of(context).cmsPage[2]['title'];
                  String content = StateContainer.of(context).cmsPage[2]['description'];
                  Navigator.pushNamed(context, TermsAndPrivacy.routeName,arguments: [title,content]);
                },
                child: Container(
                  margin: EdgeInsets.only(left: 20, top: 35),
                  child: Text(
                    Strings.termsOfServices,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  if (_scaffoldKey.currentState.isDrawerOpen) {
                    Navigator.pop(context);
                  }
                  String title = StateContainer.of(context).cmsPage[3]['title'];
                  String content = StateContainer.of(context).cmsPage[3]['description'];
                  Navigator.pushNamed(context, TermsAndPrivacy.routeName,arguments: [title,content]);
                },
                child: Container(
                  margin: EdgeInsets.only(left: 20, top: 35),
                  child: Text(
                    Strings.privacyPolicy,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void onClickBookAppointment() {
    setState(() {
      selectedMenu = SelectedMenu.BOOK_APPOINTMENT;
      Navigator.pushNamed(context, GetCategories.routeName);
    });
  }

  void onClickHowAppWorks() {
    setState(() {
      selectedMenu = SelectedMenu.HOW_APP_WORKS;
      String title = StateContainer.of(context).cmsPage[0]['title'];
      String content = StateContainer.of(context).cmsPage[0]['description'];
      Navigator.pushNamed(context, HowAppWorks.routeName,arguments: [title,content]);
    });
  }

  void onClickContact() {
    setState(() {
      selectedMenu = SelectedMenu.CONTACT;
      String title = StateContainer.of(context).cmsPage[1]['title'];
      String content = StateContainer.of(context).cmsPage[1]['description'];
      Navigator.pushNamed(context, Contact.routeName,arguments: [title,content]);
    });
  }

  void onClickSettings() {
    setState(() {
      selectedMenu = SelectedMenu.SETTINGS;
      Navigator.pushNamed(context, SettingsScreen.routeName);
    });
  }
}
