import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/utils/strings.dart';

class RegistrationScreenHeader extends StatefulWidget {
  @override
  _RegistrationScreenHeaderState createState() => _RegistrationScreenHeaderState();
}

class _RegistrationScreenHeaderState extends State<RegistrationScreenHeader> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Container(
              width: 50,
              height: 50,
              padding: EdgeInsets.all(10),
              child: Image.asset("assets/icons/icon_back.png")
          ),
        ),
        Expanded(child: Text(Strings.registration,textAlign: TextAlign.center,style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.bold
        ),)),
        GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Container(
              width: 50,
              height: 50,
              padding: EdgeInsets.all(10),
              child: Image.asset("assets/icons/icon_close.png")
          ),
        ),

      ],
    );
  }
}
