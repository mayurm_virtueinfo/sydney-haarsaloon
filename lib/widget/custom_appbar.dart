import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';

class CustomAppbar extends StatefulWidget {
  @override
  _CustomAppbarState createState() => _CustomAppbarState();
}

class _CustomAppbarState extends State<CustomAppbar> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return AppBar(
      backgroundColor: MyColors.backgroundColor,
      elevation: 0.0,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Center(
          child: Container(
              width: screenWidth/2,
              child: Image.asset("assets/icons/logo.png")
          ),
        ),
      ),
      bottom: PreferredSize(
        preferredSize: Size.square(screenWidth/2),
        child: Container(),
      ),
    );
  }
}
