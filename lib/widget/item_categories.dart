import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/ui/home/get_services.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';

class ItemCategories extends StatefulWidget {
  final Map<String,dynamic> category;
  ItemCategories({this.category});
  @override
  _ItemCategoriesState createState() => _ItemCategoriesState();
}

class _ItemCategoriesState extends State<ItemCategories> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(GetServices.routeName,arguments: [widget.category]);
      },
      child: Container(
          margin: EdgeInsets.only(
              left: 75,
              right: 75,
              top: 10,
              bottom: 10),
          child: AppButton(
            buttonText: widget.category['title'],
          )),
    );
  }
}
