import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/state_container/state_container.dart';
import 'package:sydney_haarsalon/utils/my_toast.dart';
import 'package:url_launcher/url_launcher.dart';

class AppFooter extends StatefulWidget {
  @override
  _AppFooterState createState() => _AppFooterState();
}

class _AppFooterState extends State<AppFooter> {

  _launchURL(url) async {
    if (await canLaunch(url)) {
      launch(url,forceWebView: true);
    } else {
      String message = 'Could not launch $url';
      MyToast.showToast(message,context);
    }
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      child: Column(
        children: <Widget>[
          Wrap(
            direction: Axis.horizontal,
            children: <Widget>[
              GestureDetector(
                onTap: (){
                  String fbUrl = StateContainer.of(context).socialPage[1]['url'];
                  debugPrint("isnta url : ${fbUrl}");
                  _launchURL(fbUrl);
                },
                child: Image.asset(
                  "assets/icons/icon_instagram.png",
                  width: 30,
                ),
              ),
              GestureDetector(
                onTap: (){
                  String fbUrl = StateContainer.of(context).socialPage[0]['url'];
                  debugPrint("fb url : ${fbUrl}");
                  _launchURL(fbUrl);
                },
                child: Image.asset(
                  "assets/icons/icon_facebook.png",
                  width: 30,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "VOORWAARDEN",
              style: TextStyle(fontSize: 12),
            ),
          )
        ],
      ),
    );
  }
}
