import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/ui/home/get_stylists.dart';
import 'package:sydney_haarsalon/ui/home/select_date_and_time.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';

class ItemStylist extends StatefulWidget {
  final Map<String, dynamic> stylist;
  final Map<String, dynamic> service;
  final Map<String, dynamic> category;
  ItemStylist({this.stylist,this.service,this.category});

  @override
  _ItemStylistState createState() => _ItemStylistState();
}

class _ItemStylistState extends State<ItemStylist> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
//        Navigator.of(context).pushNamed(SelectDate.routeName,arguments: [widget.stylist]);
        Navigator.of(context).pushNamed(SelectDateAndTime.routeName,arguments: [widget.stylist,widget.service,widget.category]);
      },
      child: Container(
          height: screenWidth/2,
          width: screenWidth/2,
          padding: EdgeInsets.all(20),
          child: Container(
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: MyColors.borderColor),
            width: double.infinity,
            child: Center(
              child: Text(
              widget.stylist['name'],
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            ),
          )),
    );
  }
}
