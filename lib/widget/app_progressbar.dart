import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';

class AppProgressbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox(
        width: 40,
        height: 40,
        child: CircularProgressIndicator(
          backgroundColor: MyColors.borderColor,
          valueColor: AlwaysStoppedAnimation<Color>(
              MyColors.backgroundColor),
          strokeWidth: 2,
        ),
      ),
    );
  }
}
