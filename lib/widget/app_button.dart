import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';

class AppButton extends StatefulWidget {
  String buttonText;
  EdgeInsetsGeometry margin;
  EdgeInsetsGeometry padding;
  Color backGroundColor;
  AppButton({this.buttonText,this.backGroundColor,this.margin,this.padding});

  @override
  _AppButtonState createState() => _AppButtonState();
}

class _AppButtonState extends State<AppButton> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      margin: widget.margin,
      padding: widget.padding,
      decoration: BoxDecoration(
          border: Border.all(
            color: MyColors.borderColor,
            width: 2.0,
          ),
          borderRadius: BorderRadius.circular(10),
        color: widget.backGroundColor??MyColors.backgroundColor
      ),
      width: screenWidth / 2,
      height: 50,
      child: Center(child: Text(widget.buttonText,style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),)),
    );
  }
}
