import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/ui/home/get_stylists.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:sydney_haarsalon/widget/app_button.dart';

class ItemService extends StatefulWidget {
  final Map<String, dynamic> service;
  final Map<String,dynamic> category;
  ItemService({this.service,this.category});

  @override
  _ItemServiceState createState() => _ItemServiceState();
}

class _ItemServiceState extends State<ItemService> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(GetStylists.routeName,arguments: [widget.service,widget.category]);
      },
      child: Container(
          height: 70,
          margin: EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 10),
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(
                  color: MyColors.borderColor,
                  width: 2.0,
                ),
                borderRadius: BorderRadius.circular(10),
                color: MyColors.backgroundColor),
            width: double.infinity,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(child: Text(widget.service['title'],style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),)),
                  Text("€ ${widget.service['price']}",style: TextStyle(fontSize: 18,fontWeight:FontWeight.bold),),
                ],
              ),
            ),
          )),
    );
  }
}
