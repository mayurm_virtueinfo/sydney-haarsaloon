import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';

class HomeMenuButton extends StatefulWidget {
  bool isTapped;
  Function onTap;
  String name;
  Color backgroundColor;
  String imgAssetPath;
  HomeMenuButton({this.onTap,this.name,this.backgroundColor,this.imgAssetPath,this.isTapped});

  @override
  _HomeMenuButtonState createState() => _HomeMenuButtonState();
}

class _HomeMenuButtonState extends State<HomeMenuButton> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingMenu = 30;
    double menuSize = (screenWidth / 5) + (paddingMenu * 2) - 10;
    return GestureDetector(
      onTap: widget.onTap,
      child: widget.isTapped? Container(
        decoration: BoxDecoration(
            color: widget.backgroundColor,
            border: Border.all(
              color: MyColors.borderColor,
              width: 2.0,
            ),
            borderRadius: BorderRadius.circular(10)),
        width: menuSize,
        height: menuSize,
        child: Center(
          child: Stack(
            children: <Widget>[
              Opacity(
                opacity: 0.2,
                  child: Image.asset(widget.imgAssetPath)
              ),
              Container(
                alignment: Alignment.center,
                child: Text(widget.name,style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,

                ),textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
      ):Container(
        decoration: BoxDecoration(
            color: widget.backgroundColor,
            border: Border.all(
              color: MyColors.borderColor,
              width: 2.0,
            ),
            borderRadius: BorderRadius.circular(10)),
        width: menuSize,
        height: menuSize,
        child:
        Image.asset(widget.imgAssetPath),
      ),
    );
  }
}
