import 'package:flutter/material.dart';

class MyColors{
  static Color colorConvert(String color) {
    color = color.replaceAll("#", "");
    if (color.length == 6) {
      return Color(int.parse("0xFF"+color));
    } else if (color.length == 8) {
      return Color(int.parse("0x"+color));
    }
  }

  static Color get backgroundColor => MyColors.colorConvert("#ededed");
  static Color get white => Colors.white;
  static Color get borderColor => MyColors.colorConvert("#baaa9d");
  static Color get bookedColor => MyColors.colorConvert('#1b2232');
  static Color get hintColor => MyColors.colorConvert("#c2c2c2");
  static Color get calendarDateColor => MyColors.colorConvert("#C8C8C8");
  static Color get weekendColorStyle => MyColors.colorConvert("#3F6B74");
  static Color get splshBackgroundColor => MyColors.colorConvert('#1b2232');
}