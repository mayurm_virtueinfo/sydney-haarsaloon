class Strings{
  static String login = "AANMELDEN";
  // static String registration = "ISNCHRIJVEN";
  static String registration = "INSCHRIJVEN";
  static String toMakeAnAppointment = "AFSPRAAK PLANNEN";
  static String howTheAppWorks = "ZO WERKT DE APP";
  static String contact = "CONTACT";
  static String settings = "INSTELLINGEN";
  static String termsOfServices = "SERVICEVOORWAARDEN";
  static String privacyPolicy = "PRIVACYBELEID";
  static String cutting = "KNIPPEN";
  static String coloring = "KLEUREN";
  static String cuttingAndColoring = "KNIPPEN & KLEUREN ";
  static String menu = "MENU";
  static String selectService = "SELECTEER DIENST";
  static String userNameOrEmailAddress = "GEBRUKERSNAAM OF EMAIL ADRES";
  static String password = "WACHTWOORD";
  static String confirm = "BEVESTIG";
  static String waitingList = "Wachtlijst";
  static String close = "SLUITEN";
  static String nameLastName = "NAAM + ACHTERNAAM";
  static String email = "EMAIL";
  static String mobile = "MOBIEL";
  // static String city = "STAD";
  // static String zipcode = "POSTCODE";
}