import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';

class AppStyle{
  static TextStyle stylePrefix = TextStyle(
      color: Colors.black,
      fontSize: 20
  );
  static TextStyle styleText = TextStyle(
      color: Colors.black,
      fontSize: 20
  );
  static TextStyle styleHint = TextStyle(
      color: MyColors.hintColor,fontSize: 20
  );
}