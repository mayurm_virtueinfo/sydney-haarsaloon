import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sydney_haarsalon/main.dart';
import 'package:sydney_haarsalon/service/navigation_service.dart';
import 'package:sydney_haarsalon/ui/home/waiting_list_confirm_screen.dart';

class Utility{
  static void checkForNotification(Map<String,dynamic> data){
    String title = data['title'];
    String body = data['body'];
    String screen = data['screen'];
    String id_booking = data['id_booking'];
    if(screen==null){
      return;
    }
    switch (screen) {
      case WaitingListConfirmScreen.screenName:
        locator<NavigationService>().navigateTo(WaitingListConfirmScreen.routeName,id_booking);

        /*Navigator.of(context).pushNamed(SplashScreen.routeName,
            arguments: {'storeSlug': 'ajio-coupons', 'storeName': 'AGIO'});*/
        break;
    }
  }
  static Future<String> getDeviceUUID() async {
    String deviceName;
    String deviceVersion;
    String identifier;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.model;
        deviceVersion = build.version.toString();
        identifier = build.androidId;  //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceName = data.name;
        deviceVersion = data.systemVersion;
        identifier = data.identifierForVendor;  //UUID for iOS
      }
    } on PlatformException {
      throw  Exception('Failed to get platform version');
    }

//if (!mounted) return;
    return identifier;
  }


  static bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }
}