import 'package:flutter/material.dart';
import 'package:sydney_haarsalon/utils/my_colors.dart';
import 'package:toast/toast.dart';
class MyToast{
  /*static showToast(String message){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: MyColors.borderColor,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }*/

  static showToast(String message, BuildContext context){
    Toast.show(message, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM,backgroundColor: Theme.of(context).accentColor,textColor: Colors.white,);
  }

}